   jQuery(function($) {

    var items = $(".pagination .block_paginacion");
    var numItems = items.length;
    var perPage = 16;
    var hash = location.hash.substr(1);
    hash = hash.split("-");
    var number = hash[1];

    if(number != "" && number > 0)
    {
      current_page = number;
      pageNumber = number;
    }
    else
    {
      current_page = 1;
      pageNumber = 1;
    }

    // only show the first 2 (or "first per_page") items initially
    items.slice(perPage).hide();

    $("#pag-subhome").pagination({
        items: numItems,
        itemsOnPage: perPage,
        hrefTextPrefix: '#pagina-',
        cssStyle: 'light-theme',
        currentPage : current_page,
        onInit: function(number) {
          var showFrom = perPage * (current_page - 1);
          var showTo = showFrom + perPage;

          items.hide() 
          .slice(showFrom, showTo).show();
        },
        onPageClick: function(pageNumber) { // this is where the magic happens
        // someone changed page, lets hide/show trs appropriately
        console.log(pageNumber);
        var showFrom = perPage * (pageNumber - 1);
        var showTo = showFrom + perPage;

        items.hide() // first hide everything, then show for the new page
        .slice(showFrom, showTo).show();
      }
    });
    if(numItems <= perPage){
      $(".pagination_uno").hide();
    }
});