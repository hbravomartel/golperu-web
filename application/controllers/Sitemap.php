<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	function __construct()
    {
        parent::__construct();
    }

    function index()
    {
    	$noticias = json_decode(file_get_contents('http://cmsgolperu.pncea.4cloud.co//sitemap/index?pid=1'));

    	$año = 2017;
      	for($mes = 1; $mes <= 12; $mes++)
      	{
      		$inicio = $año.'-'.$mes.'-01';
      		$fin = $año.'-'.$mes.'-31';

      		$fecha_1 = strtotime($inicio);
      		$fecha_31 = strtotime($fin);

      		$xml = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
	    	foreach($noticias as $noticia)
	    	{
	    		if(strtotime($noticia->fecha) >= $fecha_1 AND strtotime($noticia->fecha) <= $fecha_31)
	    		{
	    			$xml .= '<url>';
	    			$xml .= '<loc><![CDATA[ http://golperu.pe/'.$noticia->nota_seolink.'-'.$noticia->nota_id.' ]]></loc>';
	    			$xml .= '<lastmod><![CDATA[ '.$noticia->fecha.' ]]></lastmod>';
	    			$xml .= '<changefreq>always</changefreq>';
	    			$xml .= '<priority><![CDATA[ 0.8 ]]></priority>';
	    			$xml .= '</url>';
	    		}
	    	}

	    	$xml .= '</urlset>';

	    	file_put_contents(FCPATH.'sitemap/sitemap_contents_'.$año.'_'.$mes.'.xml', $xml);

	    	echo 'mes: '.$mes.'<br>';
	    }
	    echo 'año: '.$año.'<br>';
    }

    function sitemap_index()
    {
        $año = 2017;

      	$xml = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
          
          for($mes = 1; $mes <= 12; $mes++)
        	{
  	    	$sitemap  = 'http://golperu.pe/sitemap/sitemap_contents_'.$año.'_'.$mes.'.xml';
  	    	$fecha = strtotime($año.'-'.$mes.'-30');
  	    	$xml .= '<sitemap>';
  	    	$xml .= '<loc><![CDATA[ '.$sitemap.' ]]></loc>';
  	    	$xml .= '<lastmod><![CDATA[ '.date(DATE_ATOM, $fecha).' ]]></lastmod>';
  	    	$xml .= '</sitemap>';		  
  	    }

        $xml .= '</sitemapindex>';

        file_put_contents(FCPATH.'sitemap/sitemap_index.xml', $xml);
    }

    function update()
    {
    	$noticias = json_decode(file_get_contents('http://cmsgolperu.pncea.4cloud.co/sitemap/update?pid=1'));

    	$xml = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    	foreach($noticias as $noticia)
    	{
            $noticia->nota_seolink = preg_replace("/&#?[a-z0-9]{2,8};/i","",$noticia->nota_seolink);

    			$xml .= '<url>';
          $xml .= '<loc><![CDATA[ http://golperu.pe/'.$noticia->nota_seolink.'-'.$noticia->nota_id.' ]]></loc>';    			
    			$xml .= '<lastmod><![CDATA[ '.$noticia->fecha.' ]]></lastmod>';
    			$xml .= '<changefreq>always</changefreq>';
    			$xml .= '<priority><![CDATA[ 0.8 ]]></priority>';
    			$xml .= '</url>';
    	}

    	$xml .= '</urlset>';


    	$year = date('Y');
    	$month = date('n');
    	$ruta = FCPATH.'sitemap/sitemap_contents_'.$year.'_'.$month.'.xml';
    	
    	if(!is_file($ruta)) // si es inicio de mes
    	{
            $xmldoc = new DOMDocument();
            $xmldoc->load(FCPATH.'sitemap/sitemap_index.xml');

            $sitemap = $xmldoc->createElement('sitemap');

            $loc = $xmldoc->createElement('loc');
            $loc->appendChild($xmldoc->createCDATASection('http://golperu.pe/sitemap/sitemap_contents_'.$year.'_'.$month.'.xml'));
            $sitemap->appendChild($loc);

            $lastmod = $xmldoc->createElement('lastmod');
            $lastmod->appendChild($xmldoc->createCDATASection(date(DATE_ATOM)));
            $sitemap->appendChild($lastmod);

            $sites = $xmldoc->getElementsByTagName('sitemapindex')->item(0);
            $first_site = $sites->getElementsByTagName('sitemap')->item(0);

            $sites->insertbefore($sitemap, $first_site);

            $xmldoc->save(FCPATH.'sitemap/sitemap_index.xml');
    	}
    	else // si se tiene que actualizar el archivo de mes
    	{
            $xmldoc = new DOMDocument();
            $xmldoc->load(FCPATH.'sitemap/sitemap_index.xml');

            $lastmod = $xmldoc->createElement('lastmod');
            $lastmod->appendChild($xmldoc->createCDATASection(date(DATE_ATOM)));
            $items = $xmldoc->getElementsByTagName('sitemap')->item(0);

            $items->replaceChild($lastmod, $xmldoc->getElementsByTagName('lastmod')->item(0));

            $xmldoc->save(FCPATH.'sitemap/sitemap_index.xml');   		
    	}

    	file_put_contents($ruta, $xml);

    	echo 'listo! :) ';
    }

}

