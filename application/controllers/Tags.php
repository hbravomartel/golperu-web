<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('validador', 'tag');
        $this->load->model(array('Copa_Movistar_modelo', 'Portadas_modelo', 'Tags_modelo'));
        
	}

	function index($tag)
    {
        $data = $this->data;
        
        // Conseguir todas los nids del tag
        $nids = $this->Tags_modelo->get_all_by_tag($tag);
        // if(empty($nids))
        // {
        //     redirect('/copa-movistar');
        // }

        $noticias = $this->Tags_modelo->get_noticias_by_ids($nids, $tag);
        $data['noticias'] = $noticias;
        $data['tag'] = $this->Tags_modelo->get_nombre_by_tag($tag);

        $titulo_seccion = 'Tags';

        // Meta Tags
        $data['meta_title'] = $titulo_seccion;
        $data['seccion'] = $titulo_seccion;

        $this->load->view('templates/header', $data);
        $this->load->view('tags_subhome', $data);
        $this->load->view('templates/footer', $data);
    }

    

}