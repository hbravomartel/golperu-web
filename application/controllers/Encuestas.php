<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Encuestas extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->helper('validador', 'tag');
        $this->load->model(array('Encuesta_modelo'));
        
	}

    public function votar()
    {

    $encuesta_id = $this->input->post('encuesta', true);
    $opcion_id = $this->input->post('opcion', true);

    $lifetime = 600;
    session_set_cookie_params($lifetime);
    session_start();

    //if($_SESSION['encuesta_flag'] === FALSE or isset($_SESSION['encuesta_flag']))
    if(!isset($_COOKIE['encuesta']))
    {
        $votacion = $this->Encuesta_modelo->votar($opcion_id, $encuesta_id);
        if($votacion == TRUE)
        {            
            $_SESSION['encuesta_flag'] = TRUE;
            $_SESSION['encuesta_id'] = time();
            setcookie('encuesta',session_id(),time()+$lifetime);
            echo 1;
        }
    }
    else
    {
        echo 0;
    }

    
    

    }

    function actualizar_encuesta()
    {
        $encuesta = $this->Encuesta_modelo->get_last_encuesta();
        $data['encuesta'] = $encuesta;
        $this->load->view('includes/encuesta', $data);
    }


}