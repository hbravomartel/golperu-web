<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
set_time_limit(0);

class Futbol_Local_service extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('read_htm');
		$this->load->helper(array('noticias', 'tag'));
	}

	function index()
	{
		// Conseguir el json del motor
		error_reporting(E_ALL);
		ini_set('display_errors', 1);

		$noticias = json_decode(file_get_contents($this->config->item('cms_api_local')));
		$nids = array();

		// Validar json antes de guardarlo
		$error = "";
		
		// Json es valido
			if(is_object($noticias) || empty($noticias))
			{
				$error = "JSON inválido, no hay objetos.";
			}
			else
			{
				// Validar si el campo nid y titulo existen
				foreach ($noticias as $noticia)
					{
						$nid = $noticia->nid;
						$titular = $noticia->titular;
						
						if(is_numeric($nid))
						{
							if(!empty($titular))
							{
								if(!is_string($titular)){											
									$error .= "Título incorrecto en la noticia : ".$nid;										
								}
							}else{
								$error .= "Título vacío en la noticia : ".$nid;		
							}
						}
						
						else
						{						
							$error .= "Nid vacío";
						}
					}			
			}
		
		$error .= json_last_error();

		if(!empty($error))
		{
			$error .= ' | seccion: noticias';
			// Enviar correo avisando informando el error
			// $this->load->library('email');
			// $this->email->from('hellen992@gmail.com', 'Desarrollo CRP');
			// $this->email->to('hellen992@gmail.com');	
			// $this->email->subject('Error Json - GolPerú');
			// $this->email->message($error);		
			// $this->email->send();
		}
		else
		{
			// Crear el directorio de noticias si no existe
			if (!is_dir(FCPATH . 'json/noticias/')) 
			{
				$antiguo = umask(0);
				mkdir(FCPATH . 'json/noticias/', 0775, TRUE);
				umask($antiguo);
			}
			
			// Guardar el json en el servidor
			file_put_contents(noticias_path_lista('futbol_local'), json_encode($noticias));

			// Guardar notas individuales en físico
			foreach ($noticias as $key => $noticia)
			{
				$categoria_nids = array();
				$seccion_nids = array();
				$nid = $noticia->nid;
				$dir = noticias_path($nid);
				$categoria = url_title(convert_accented_characters($noticia->categoria), '-', TRUE);
				$seccion = url_title(convert_accented_characters($noticia->seccion), '-', TRUE);
				$key = NULL;

				//Paso los datos generales
            	$categoria_id = $noticia->cid;
            	$competition_id = $noticia->competition_id;
            	$season = $noticia->season;

            	//Guardar datos del servicio
				$json_info = array('categoria_id' => $categoria_id, 'competition_id'=>$competition_id, 'season' => $season);			
				file_put_contents(categoria_path_info($seccion, $categoria), json_encode($json_info));
				
				//////////////////////////////////////////////////////////////////
				// Crear directorio de secciones
				//////////////////////////////////////////////////////////////////

				if (!is_dir(FCPATH . 'json/noticias/'.$seccion)) 
				{
					$antiguo = umask(0);
					mkdir(FCPATH . 'json/noticias/'.$seccion, 0775, TRUE);
					umask($antiguo);
				}

				//////////////////////////////////////////////////////////////////
				// Crear directorio de categorias
				//////////////////////////////////////////////////////////////////
				
				if (!is_dir(FCPATH . 'json/noticias/'.$seccion.'/'.$categoria)) 
				{
					$antiguo = umask(0);
					mkdir(FCPATH . 'json/noticias/'.$seccion.'/'.$categoria, 0775, TRUE);
					umask($antiguo);
				}
				
				/*** SECCION ****/
				// Verifica si ya hay un json de nids en la categoria, si existe lo carga en el array

				if (file_exists(seccion_path_nid($seccion, $categoria))) 
				{
					$seccion_arr = json_decode(file_get_contents(seccion_path_nid($seccion, $categoria)));
					if(is_array($seccion_arr))
					{
						$seccion_nids = $seccion_arr;
					}
					
				}

				// Verifica que los nids no se repitan
				// $key_new = array_search($nid, $tag_nids);
				// 			if ($key_new !== NULL)
				// 			{
				// 				unset($tag_nids[$key]); 
								
				// 			}
				$seccion_nids[] = $nid;	
				$seccion_nids = array_unique($seccion_nids);

				// Guarda los nids nuevos en el json (limite 500 históricos)
				file_put_contents(seccion_path_nid($seccion), json_encode(array_slice($seccion_nids, -500)));


				/*** CATEGORIA ****/
				// Verifica si ya hay un json de nids en la categoria, si existe lo carga en el array

				if (file_exists(categoria_path_nid($seccion, $categoria))) 
				{
					$categoria_arr = json_decode(file_get_contents(categoria_path_nid($seccion, $categoria)));
					if(is_array($categoria_arr))
					{
						$categoria_nids = $categoria_arr;
					}
					
				}

				// Verifica que los nids no se repitan
				// if (!in_array($nid, $categoria_nids))
				// {
				// 	$categoria_nids[] = $nid;	
				// }
				$categoria_nids[] = $nid;	
				$categoria_nids = array_unique($categoria_nids);
				

				// Guarda los nids nuevos en el json (limite 500 históricos)
				file_put_contents(categoria_path_nid($seccion, $categoria), json_encode(array_slice($categoria_nids, -500)));



				//////////////////////////////////
				// Guardar las notas individuales
				////////////////////////////////// 
				if (!is_dir($dir)) 
				{
					// Crear directorio con permisos de escritura para todos
	            	$antiguo = umask(0);
					mkdir($dir, 0775, TRUE);
					umask($antiguo);
				}

	            if (!is_dir($dir)) 
	            {
	            	// Notifica si hay un error al crear el directorio
	                echo "Directorio no existe: " . $dir . "<br/>";
	            }

	            // Crear htms y reportarlo
	            echo "Codigo de nota a procesar: <b>" . $nid . "</b><br/>";

	            foreach ($noticia as $key => $value) 
	            {
	            	if ($key === "titular") 
	            	{
	            		echo "--> Procesando nota: <b>".$value."</b><br/>";
	            	}


	            	if($key === "tags" AND !empty($value))
	            	{
	            		$x = 0;
	            		foreach($value as $v)
	            		{
	            			unset($tag_nids);
	            			$tag_nids = array();
	            			// $v = url_title(convert_accented_characters($v), '-', TRUE);
	            			// crear directorio con el nombre del tag
	            			if(!is_dir(FCPATH.'/json/tags/'.$v))
	            			{
	            				$antiguo = umask(0);
								mkdir(FCPATH.'/json/tags/'.$v, 0775, TRUE);
								umask($antiguo);
	            			}
	            			// verificar si existe el archivo nids.json en el tag
	            			if(file_exists(tag_path_nid($v)))
	            			{
	            				$file = json_decode(file_get_contents(tag_path_nid($v)));
	            				if(is_array($file))
	            				{
	            					$tag_nids = json_decode(file_get_contents(tag_path_nid($v)));
	            				}
	            			}

	            			// Verifica que los nids no se repitan
							// if (!in_array($nid, $tag_nids))
							// {
							// 	$tag_nids[] = $nid;	
							// }
							$key_new = array_search($nid, $tag_nids);
							if ($key_new !== NULL)
							{
								unset($tag_nids[$key_new]); 
								
							}
							$tag_nids[] = $nid;	

							//Guardar nombre del tag
							$tag_nombre = $noticia->tagsnombre[$x];
							file_put_contents(tag_path_nombre($v), $tag_nombre);

							// Guarda los nids nuevos en el json (limite 500 históricos)
							file_put_contents(tag_path_nid($v), json_encode(array_slice($tag_nids, -100)));
							$x++;
	            		}
	            	}

	            	// El array/objeto guardarlo como json
	            	if ($key === "foto" || $key === "fotos" || $key === "video" || $key === "videos" || $key === "audio" || $key === "gif" || $key === "tags")
	            	{
	            		// Validar que el valor no sea nulo, vacío o FALSE
	            		// if (!empty(array_filter((array)$value)))
	            		// {
		            		file_put_contents($dir.$nid.$key.'.htm', json_encode($value));
		            		chmod($dir.$nid.$key.'.htm', 0755);
	            		// }
	            		
	            	}
	            	// Crear htms para el resto de elementos
	            	else
	            	{
	            		file_put_contents($dir . $nid . $key . '.htm', $value); 
	                	chmod($dir . $nid . $key . '.htm', 0755);
	            	}
	                
				}
			}

			// Guardar array con todos los nids en el json
			// file_put_contents(noticias_path_all(), json_encode($nids));

		}
		
	}

}