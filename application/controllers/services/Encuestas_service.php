<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
set_time_limit(0);

class Encuestas_service extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('read_htm');
		$this->load->model('Encuesta_modelo');
		$this->load->helper(array('encuestas', 'tag'));
	}

	function index()
	{
		$cid = $this->uri->segment(4);

		// Conseguir el json del motor
		error_reporting(E_ALL);
		ini_set('display_errors', 1);

		if(!empty($cid))
		{
			$noticias = json_decode($this->Encuesta_modelo->get_encuesta_by_programa_json($cid));
		}
		else
		{
			$noticias = json_decode($this->Encuesta_modelo->get_encuesta_json());
		}
		// var_dump($noticias);
		$nids = array();

		// Validar json antes de guardarlo
		$error = "";
		
		// Json es valido
		if(empty($noticias))
		{
			$error = "JSON inválido, no hay objetos.";
		}
		else
		{
			// Validar si el campo nid y titulo existen
			foreach ($noticias as $noticia)
				{
					$nid = $noticia->encuesta_id;
					$titular = $noticia->encuesta_pregunta;
					
					if(is_numeric($nid))
					{
						if(!empty($titular))
						{
							if(!is_string($titular)){											
								$error .= "Título incorrecto en la noticia : ".$nid;										
							}
						}else{
							$error .= "Título vacío en la noticia : ".$nid;		
						}
					}
					
					else
					{						
						$error .= "Nid vacío";
					}
				}			
		}
		
		$error .= json_last_error();

		if(!empty($error))
		{
			$error .= ' | seccion: noticias';
			// Enviar correo avisando informando el error
			// $this->load->library('email');
			// $this->email->from('hellen992@gmail.com', 'Desarrollo CRP');
			// $this->email->to('hellen992@gmail.com');	
			// $this->email->subject('Error Json - GolPerú');
			// $this->email->message($error);		
			// $this->email->send();
		}
		else
		{
			// Crear el directorio de especiales live si no existe
			if (!is_dir(FCPATH . 'json/encuestas/')) 
			{
				$antiguo = umask(0);
				mkdir(FCPATH . 'json/encuestas/', 0775, TRUE);
				umask($antiguo);
			}

			$categoria = url_title(convert_accented_characters($noticias[0]->programa_nombre), '-', TRUE);

			if (!is_dir(FCPATH . 'json/encuestas/'.$categoria)) 
			{
				$antiguo = umask(0);
				mkdir(FCPATH . 'json/encuestas/'.$categoria, 0775, TRUE);
				umask($antiguo);
			}

			// Guardar el json en el servidor
			file_put_contents(encuestas_path_lista($categoria), json_encode($noticias));

			// Verificar si ya existe un json de nids
			if (file_exists(encuestas_path_all($categoria)))
			{
				$nids = json_decode(file_get_contents(encuestas_path_all($categoria)));
			}

			// Guardar notas individuales en físico
			foreach ($noticias as $noticia)
			{
				$nid = $noticia->encuesta_id;
				$dir = encuestas_path($nid);

				//////////////////////////////////////////////////////
				// Crear json de NIDS para tener un archivo histórico
				//////////////////////////////////////////////////////
				
				// Verificar que los nids en el json no sean repetidos
				if (!in_array($noticia->encuesta_id, $nids)) 
				{
					$nids[] = $noticia->encuesta_id;
				}

				//////////////////////////////////
				// Guardar las notas individuales
				////////////////////////////////// 
				if (!is_dir($dir)) 
				{
					// Crear directorio con permisos de escritura para todos
	            	$antiguo = umask(0);
					mkdir($dir, 0775, TRUE);
					umask($antiguo);
				}

	            if (!is_dir($dir)) 
	            {
	            	// Notifica si hay un error al crear el directorio
	                echo "Directorio no existe: " . $dir . "<br/>";
	            }

	            // Crear htms y reportarlo
	            echo "Codigo de nota a procesar: <b>" . $nid . "</b><br/>";

	            foreach ($noticia as $key => $value) 
	            {
	            	
	            	if ($key === "pregunta_nombre") 
	            	{
	            		echo "--> Procesando nota: <b>".$value."</b><br/>";
	            	}

	            	// El array/objeto guardarlo como json
	            	if ($key === "opciones")
	            	{
	            		// Validar que el valor no sea nulo, vacío o FALSE
	            		
		            		file_put_contents($dir.$nid.$key.'.htm', json_encode($value));
		            		chmod($dir.$nid.$key.'.htm', 0755);
	            		
	            		
	            	}
	            	// Crear htms para el resto de elementos
	            	else
	            	{
	            		file_put_contents($dir . $nid . $key . '.htm', $value); 
	                	chmod($dir . $nid . $key . '.htm', 0755);
	            	}
	                
				}
			}

			// Guardar array con todos los nids en el json
			file_put_contents(encuestas_path_all($categoria), json_encode($nids));


			//Actualizar json
			$noticias = array_reverse($noticias);
			$encuesta_nid = $noticias[0]->encuesta_id;
			$encuesta_query = $this->Encuesta_modelo->get_encuesta_by_id($encuesta_nid);
			$opciones = array();
			$x = 1;

		

			foreach ($encuesta_query->opciones as $k) {
				$opciones[] =  array('opciones_id' => $k->opcion_id, 'total' => '0', 'opcion_nombre' => $k->opcion_nombre);
			}

		
			
			$rutaEncuesta = 'json/encuestas/encuesta.json';
			$json = json_encode($opciones, JSON_PRETTY_PRINT);
			  
			  $fh = fopen($rutaEncuesta, 'w+') or die("Error al abrir fichero de salidas");
			  fwrite($fh, $json);
			  fclose($fh);

			
		}
		
	}

}