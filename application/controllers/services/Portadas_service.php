<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portadas_service extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('portada'));
	}

	function index()
	{

		// Conseguir el json del motor
		$noticias = json_decode(file_get_contents($this->config->item('cms_api_portadas')));
		$nids = array();

		// Validar json antes de guardarlo
		$error = "";
		
		// Json es valido
			if(is_object($noticias) || empty($noticias))
			{
				$error = "JSON inválido, no hay objetos.";
			}
			
			else
			{
				// Validar si el campo nid y titulo existen
				foreach ($noticias as $noticia)
					{
						$nid = $noticia->nid;
						$titular = $noticia->titular;
						
						if(is_numeric($nid))
						{
							if(!empty($titular))
							{
								if(!is_string($titular)){											
									$error .= "Título incorrecto en la noticia : ".$nid;										
								}
							}else{
								$error .= "Título vacío en la noticia : ".$nid;		
							}
						}
						
						else
						{						
							$error .= "Nid vacío";
						}
					}			
			}
		
		$error .= json_last_error();
		
		if(!empty($error))
		{
			// Enviar correo avisando informando el error
			// $error .= ' | seccion: portada';
			// $this->load->library('email');
			// $this->email->from('desarrollo@crp.pe', 'Desarrollo CRP');
			// $this->email->to('aloo@crp.pe');
			// $this->email->cc('hbravo@crp.pe');
			// $this->email->bcc('iaguilar@crp.pe');		
			// $this->email->subject('Error Json - moda');
			// $this->email->message($error);		
			// $this->email->send();	
		}
		else
		{
			// Crear el directorio de noticias si no existe
			if (!is_dir(FCPATH . 'json/portadas/')) 
			{
				$antiguo = umask(0);
				mkdir(FCPATH . 'json/portadas/', 0775, TRUE);
				umask($antiguo);
			}
			
			// Guardar el json en el servidor
			file_put_contents(portadas_path_lista(), json_encode($noticias));

			// Verificar si ya existe un json de nids
			if (file_exists(portadas_path_all()))
			{
				$nids = json_decode(file_get_contents(portadas_path_all()));
			}

			// Guardar notas individuales en físico
			foreach ($noticias as $noticia)
			{
				$nid = $noticia->nid;
				$dir = portadas_path($nid);

				//////////////////////////////////////////////////////
				// Crear json de NIDS para tener un archivo histórico
				//////////////////////////////////////////////////////
				
				// Verificar que los nids en el json no sean repetidos
				if (!in_array($noticia->nid, $nids)) 
				{
					$nids[] = $noticia->nid;
				}

				//////////////////////////////////
				// Guardar las notas individuales
				////////////////////////////////// 
				if (!is_dir($dir)) 
				{
					// Crear directorio con permisos de escritura para todos
	            	$antiguo = umask(0);
					mkdir($dir, 0775, TRUE);
					umask($antiguo);
				}

	            if (!is_dir($dir)) 
	            {
	            	// Notifica si hay un error al crear el directorio
	                echo "Directorio no existe: " . $dir . "<br/>";
	            }

	            // Crear htms y reportarlo
	            echo "Codigo de nota a procesar: <b>" . $nid . "</b><br/>";

	            foreach ($noticia as $key => $value) 
	            {
	            	
	            	if ($key === "titular") 
	            	{
	            		echo "--> Procesando nota: <b>".$value."</b><br/>";
	            	}

	            	// El array/objeto guardarlo como json
	            	if ($key === "foto")
	            	{
	            		// Validar que el valor no sea nulo, vacío o FALSE
	            		if (!empty(array_filter((array)$value)))
	            		{
		            		file_put_contents($dir.$nid.$key.'.htm', json_encode($value));
		            		chmod($dir.$nid.$key.'.htm', 0755);
	            		}
	            		
	            	}
	            	// Crear htms para el resto de elementos
	            	else
	            	{
	            		file_put_contents($dir . $nid . $key . '.htm', $value); 
	                	chmod($dir . $nid . $key . '.htm', 0755);
	            	}
	                
				}
			}

			// Guardar array con todos los nids en el json
			file_put_contents(portadas_path_all(), json_encode($nids));
		}
	}

}