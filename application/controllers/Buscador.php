<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buscador extends MY_Controller {
 
	function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$data = $this->data;
	
    	// Meta Tags
		$data['surl'] = base_url().uri_string();
        $data['stitle'] = 'Buscador';

        // meta data
        $data['meta_title'] = 'Buscador';
        $data['meta_url'] = base_url().uri_string();

        $this->load->view('templates/header', $data);
        $this->load->view('buscador', $data);
        $this->load->view('templates/footer', $data);
	}
		

}
