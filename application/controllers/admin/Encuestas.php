<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'Dashboard.php';

class Encuestas extends Dashboard
{
  private $__path_view = 'admin/encuestas/';

	public function __construct()
	{
		parent::__construct();
    $this->setRol(3);
    $this->masterpage->addContentPage($this->__path_view . 'menu', 'Header', array());
    $this->load->model('Encuesta_modelo');
	}

  // listar encuestas
	public function index()
	{
    $data = array(
      'encuestas' => $this->Encuesta_modelo->getAll(),
    ); 

    $this->masterpage->addContentPage($this->__path_view . 'index', 'content', $data);
    $this->masterpage->show();
	}

  function nuevo()
  {
    $data = array();
    $data['programas'] = $this->Encuesta_modelo->get_programas();
    $this->masterpage->addContentPage($this->__path_view . 'nuevo', 'content', $data);
    $this->masterpage->show();
  }

  function procesar()
  {
    $data = array(
      'programa_id' => $this->input->post('programa_id'),
      'pregunta' => $this->input->post('pregunta'),
      'opciones' => $this->input->post('opcion'),
      );


    if($this->Encuesta_modelo->save($data));
    {
      $this->session->set_flashdata('msg', 'Encuesta Guardado');
      header('Location: /admin/encuestas/index');
      // redirect('admin/moderador/index');
    }
  }

  function eliminar($id_encuesta)
  {
    if($this->Encuesta_modelo->deleteById($id_encuesta))
    {
      $this->session->set_flashdata('msg', 'Encuesta Eliminada');
      header('Location: /admin/encuestas/index');
      // redirect('admin/moderador/index');
    }
  }

  function activar($id_encuesta)
  {
    if($this->Encuesta_modelo->activar($id_encuesta))
    {
      $this->session->set_flashdata('msg', 'Encuesta Activada');
      header('Location: /admin/encuestas/index');
    }
  }

  function resultados($id_encuesta)
  {
    $this->masterpage->addContentPage($this->__path_view . 'resultados', 'content', $data);
    $this->masterpage->show();
  }

}
