<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
    parent::__construct();
  }

  public function index()
  {
    /*if ($_POST) 
    {
      $username = $this->input->post('username', true);
      $password = $this->input->post('password', true);
      $this->load->model('moderador_modelo');
      $credential = $this->moderador_modelo->validCredential($username, $password);
      if ($credential) 
      {
        $this->load->library('session');
        $this->session->set_userdata('mid', $credential->mid);
        $this->session->set_userdata('username', $username);
        $this->session->set_userdata('password', $password);
        $this->session->set_userdata('moderador_rol', (int)$credential->rid);
        header('Location: /admin/dashboard');
      }
    }*/

    $this->load->view('admin/login');
  }

  function login_form()
  { 

    if ($_POST) 
      {
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $this->load->model('moderador_modelo');
        $credential = $this->moderador_modelo->validCredential($username, $password);
        if ($credential) 
        {
          $this->load->library('session');
          $this->session->set_userdata('mid', $credential->mid);
          $this->session->set_userdata('username', $username);
          $this->session->set_userdata('password', $password);
          $this->session->set_userdata('moderador_rol', (int)$credential->rid);
          header('Location: /admin/dashboard');
        }
      }
  }
}


