<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'Security.php';

class Dashboard extends Security
{
  private $__path_view = 'admin/dashboard/';

	public function __construct()
	{
		parent::__construct();
	    $this->load->library('masterpage');
	    $this->masterpage->setMasterPage('admin/default');
	}

	public function index()
	{
	    $this->masterpage->addContentPage($this->__path_view . 'index', 'content', array());
	    $this->masterpage->show();
	}
}
