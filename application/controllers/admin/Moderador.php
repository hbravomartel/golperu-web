<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'Dashboard.php';

class Moderador extends Dashboard
{
  private $__path_view = 'admin/moderador/';

	public function __construct()
	{
		parent::__construct();
    $this->setRol(2);
    $this->masterpage->addContentPage($this->__path_view . 'menu', 'Header', array());
	}

	public function index()
	{
    $data = array(
      'moderadores' => $this->moderador_modelo->getAll(),
    );

    $this->masterpage->addContentPage($this->__path_view . 'index', 'content', $data);
    $this->masterpage->show();
	}

  public function nuevo()
  {
    $data = array();
    $this->masterpage->addContentPage($this->__path_view . 'nuevo', 'content', $data);
    $this->masterpage->show();
  }

  public function editar($mid)
  {
    $moderador = $this->moderador_modelo->getById($mid);

    $data['moderador'] = $this->moderador_modelo->getById($mid);
    $data['mid'] = $mid;
    $this->masterpage->addContentPage($this->__path_view . 'nuevo', 'content', $data);
    $this->masterpage->show();
      
  }

  public function eliminar($mid)
  {
    if($this->moderador_modelo->deleteById($mid))
    {
      $this->session->set_flashdata('msg', 'Usuario Eliminado');
      header('Location: /admin/moderador/index');
      // redirect('admin/moderador/index');
    }
  }

  public function save()
  {
    $mid  = $this->input->post('mid');
    if (empty($mid))
    {
      $mid = 0;
    }

    $data = array(
      'username' => $this->input->post('username'),
      'password' => $this->input->post('password'),
      'rid' => $this->input->post('rid'),
      'mid' => $mid
      );

    if($this->moderador_modelo->save($data));
    {
      $this->session->set_flashdata('msg', 'Usuario Guardado');
      header('Location: /admin/moderador/index');
      // redirect('admin/moderador/index');
    }
  }
}