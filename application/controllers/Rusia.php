<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rusia extends MY_Controller
{
    private $seccion = 'rusia-2018';

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('validador', 'tag');
        $this->load->model(array('Rusia_modelo', 'Portadas_modelo', 'Tags_modelo'));
        
	}

	function index()
    {
        $data = $this->data;
        
       // $noticias = $this->Rusia_modelo->get_noticias(105);
        $seleccion_nacional = $this->Tags_modelo->get_noticias_by_tags('seleccion_nacional');

        $data['noticias_slider'] = array_slice($noticias, 0, 4);
        $data['noticias'] = array_slice($noticias, 5, 100);

        $titulo_seccion = 'Rusia 2018';

        // Meta Tags
        $data['meta_title'] = $titulo_seccion;
        $data['seccion'] = $titulo_seccion;
        $data['nav_subhome'] = getMenuSeccion(15, $titulo_seccion);
        $data['categoria'] = $titulo_seccion;

        $this->load->view('templates/header', $data);
        $this->load->view('seccion_subhome', $data);
        $this->load->view('templates/footer', $data);
    }

    function interna($nid)
    {
        $data = $this->data;

        $noticia = $this->Rusia_modelo->get_noticia_by_id($nid);

        if(!empty($noticia))
        {
            // Verifica que las URLs no cambien
            $linkseo = $noticia->linkseo;
            $url_real = '/'.$linkseo.'-'.$nid;

            // Compara la URL ingresada con la real
            if(!empty($_SERVER['QUERY_STRING']))
            {
                if($url_real.'?'.$_SERVER['QUERY_STRING'] != $_SERVER['REQUEST_URI'])
                {
                    redirect($url_real.'?'.$_SERVER['QUERY_STRING'], 'refresh');
                }
            }
            else
            {
                if($url_real != $_SERVER['REQUEST_URI'])
                {
                    redirect($url_real, 'refresh');
                }
            }

            $data['noticia'] = $noticia;

            // tags
            foreach($noticia->tags as $tag)
            {
                $nombre = tag_get_elem($tag, 'nombre.htm');
                $tags_arr[$nombre] = get_tag_url($tag);
            }

            if(!empty($tags_arr))
            {
                $data['tags'] = $tags_arr;
            }

            // noticias relacionadas
            $relacionadas_id = $this->Tags_modelo->get_noticias_by_tags($noticia->tags);
            if(!empty($relacionadas_id))
            {
                $relacionadas = $this->Rusia_modelo->get_noticias_by_ids($relacionadas_id);
                foreach($relacionadas as $key => $value)
                {
                    if(!empty($value))
                    {
                        if($value->nid === $noticia->nid){unset($relacionadas[$key]);}
                    }
                    else
                    {
                        unset($relacionadas[$key]);
                    }   
                }
                $data['relacionadas'] = array_slice($relacionadas, 0, 4);
            }

            // Social Share
            $data['surl'] = base_url().uri_string();
            $data['stitle'] = $noticia->titular;

            // meta data
            $titular = htmlentities($noticia->titular);
            $data['meta_title'] = $titular;
            $data['meta_description'] = limpiar_detalle($noticia->desarrollo).'...';
            $data['meta_image'] = $noticia->fotoportada;

            $titulo_seccion = humanize($noticia->seccion);
            $data['nav_subhome'] = getMenuSeccion(15, $titulo_seccion);
            $data['nav_seccion'] = humanize($noticia->seccion);

            // Meta Tags
            if(!empty($noticia->tags))
            {
                $num = 1;
                $keywords = '';
                foreach($noticia->tags as $tag)
                {
                    $keywords .= $num == count($noticia->tags) ? humanize($tag, '-') : humanize($tag, '-').', ';
                    $num++;
                }
                $data['meta_keywords'] = $keywords;
            } 

            $this->load->view('templates/header', $data);
            $this->load->view('notas_interna', $data);
            $this->load->view('templates/footer', $data);
        }
        else
        {
            redirect('/rusia-2018', 'location', 301);
        }   
    }


}