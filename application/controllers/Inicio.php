<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends MY_Controller {

        
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Copa_Movistar_modelo', 'Portadas_modelo', 'Ligas_Internacionales_modelo', 'Rusia_modelo', 'Encuesta_modelo'));
	
        }

    function index()
	{
		$data = $this->data;
		$data['noticias'] = $this->Portadas_modelo->get_portadas(5);
                
		$noticias_copa_movistar = $this->Copa_Movistar_modelo->get_noticias_by_categoria('noticias', 7);
		$noticias_copa_slider = array_slice($noticias_copa_movistar, 0,1);
		$noticias_copa = array_slice($noticias_copa_movistar, 1,6);
		$data['noticias_copa_slider'] = $noticias_copa_slider;
		$data['noticias_copa'] = $noticias_copa ;
		$data['noticias_ligas'] = $this->Ligas_Internacionales_modelo->get_noticias_by_seccion(6);
		$data['noticias_mundial'] = $this->Rusia_modelo->get_noticias_by_seccion(6);

		
		$encuesta = $this->Encuesta_modelo->get_last_encuesta();
		$data['encuesta'] = $encuesta;
        // $data['jugadores'] = $this->Jugadores_modelo->get_all();
	
		$this->load->view('templates/header', $data);
		$this->load->view('portada', $data);
		$this->load->view('templates/footer', $data);
	}

	function widget()
	{
		$data = $this->data;
		$data['noticias'] = $this->Portadas_modelo->get_portadas(5);

		$noticias_copa_movistar = $this->Copa_Movistar_modelo->get_noticias_by_categoria('noticias', 7);
		$noticias_copa_slider = array_slice($noticias_copa_movistar, 0,1);
		$noticias_copa = array_slice($noticias_copa_movistar, 1,6);
		$data['noticias_copa_slider'] = $noticias_copa_slider;
		$data['noticias_copa'] = $noticias_copa ;
		$data['noticias_ligas'] = $this->Ligas_Internacionales_modelo->get_noticias_by_seccion(6);
		$data['noticias_mundial'] = $this->Rusia_modelo->get_noticias_by_seccion(6);

		
		$encuesta = $this->Encuesta_modelo->get_last_encuesta();
		$data['encuesta'] = $encuesta;
	
		$this->load->view('templates/header_widget', $data);
		$this->load->view('widget', $data);
		$this->load->view('templates/footer', $data);
	}

	function index_rusia()
	{
		$data = $this->data;
		$data['noticias'] = $this->Portadas_modelo->get_portadas(5);
                
		$noticias_copa_movistar = $this->Copa_Movistar_modelo->get_noticias_by_categoria('noticias', 7);
		$noticias_copa_slider = array_slice($noticias_copa_movistar, 0,1);
		$noticias_copa = array_slice($noticias_copa_movistar, 1,6);
		$data['noticias_copa_slider'] = $noticias_copa_slider;
		$data['noticias_copa'] = $noticias_copa ;
		$data['noticias_ligas'] = $this->Ligas_Internacionales_modelo->get_noticias_by_seccion(6);
		$data['noticias_mundial'] = $this->Rusia_modelo->get_noticias_by_seccion(6);

		
		$encuesta = $this->Encuesta_modelo->get_last_encuesta();
		$data['encuesta'] = $encuesta;
        // $data['jugadores'] = $this->Jugadores_modelo->get_all();
	
		$this->load->view('templates/header', $data);
		$this->load->view('portada_rusia', $data);
		$this->load->view('templates/footer', $data);
	}

	
}
