<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// subfolder por categoria
if(!function_exists('encuestas_path_lista'))
{
	function encuestas_path_lista($categoria)
	{
		return FCPATH.'json/encuestas/'.$categoria.'/lista.json';
	}
}

// Json con nids (histórico)
if(!function_exists('encuestas_path_all'))
{
	function encuestas_path_all($categoria)
	{
		return FCPATH .'json/encuestas/'.$categoria.'/nids.json';
	}
}

// Crea el path para cada nota individual
if(!function_exists('encuestas_path'))
{
	function encuestas_path($nid)
	{
		$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
		return FCPATH . 'json/encuestas/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
	}
}

function programa_encuesta_path($categoria)
{
	return FCPATH.'json/encuestas/'.$categoria;
}

function programa_encuesta_path_nid($categoria)
{
	return FCPATH.'json/encuestas/'.$categoria.'/nids.json';
}