<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	// Crea el path para cada noticia individual
	if(!function_exists('categorias_path'))
	{
		function categorias_path($nid)
		{
			$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
			return FCPATH . 'json/categorias/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
		}
	}	

	// Json con las últimas categorias
	if(!function_exists('categorias_path_lista')) 
	{  
	  function categorias_path_lista() 
	  {
	    return FCPATH . 'json/categorias/lista.json';
	  }
	}

		// Json con las últimas categorias
	if(!function_exists('categorias_path_all')) 
	{  
	  function categorias_path_all() 
	  {
	    return FCPATH . 'json/categorias/all.json';
	  }
	}

	function categorias_path_nids()
	{
		return FCPATH . 'json/categorias/nids.json';
	}


	