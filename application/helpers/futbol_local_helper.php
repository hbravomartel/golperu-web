<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	// Crea el path para cada noticia individual
	if(!function_exists('noticias_path'))
	{
		function noticias_path($nid)
		{
			$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
			return FCPATH . 'json/noticias/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
		}
	}	

	// Json con las últimas noticias
	

		// Json con las últimas noticias
	if(!function_exists('noticias_path_all')) 
	{  
	  function noticias_path_all() 
	  {
	    return FCPATH . 'json/noticias/all.json';
	  }
	}

	function noticias_path_nids()
	{
		return FCPATH . 'json/noticias/nids.json';
	}


	if(!function_exists('seccion_path_nid'))
	{
		function seccion_path_nid($seccion)
		{
			return FCPATH.'json/noticias/'.$seccion.'/nids.json';
		}
	}


	if(!function_exists('categoria_path_nid'))
	{
		function categoria_path_nid($seccion, $categoria)
		{
			return FCPATH.'json/noticias/'.$seccion.'/'.$categoria.'/nids.json';
		}
	}

	// Crea el path para cada noticia individual
	if(!function_exists('nota_path'))
	{
		function nota_path($seccion, $nid)
		{			
			$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
			return FCPATH . 'json/'.$seccion.'/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
		}
	}

	function getMenuSeccion($seccion_id, $seccion_title)
	{
		$nav = '';
		switch ($seccion_id) {
			case 11:
				$nav = '<section class="seccion-head">
					        <div class="container">
					          <h2>'.$seccion_title.'</h2>
					          <div class="list">
					            <ul>
					              <li><a href="/copa-movistar/noticias">Noticias </a></li>
					              <li><a href="#">Estadísticas </a></li>
					              <li><a href="#">Goles de la fecha </a></li>
					              <li><a href="#">  Fixture </a></li>
					            </ul>
					          </div>
					        </div>
					      </section>';
				break;
			
			default:
				# code...
				break;
		}

		return $nav;

	}