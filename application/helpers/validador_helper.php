<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('toAscii'))
{
	function toAscii($str, $replace=array(), $delimiter='-')
	{
		if( !empty($replace) )
		{
			$str = str_replace((array)$replace, ' ', $str);
		}
		/*setlocale(LC_CTYPE, 'en_US.UTF-8');

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);*/
		$clean = convert_accented_characters($str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}
}

// Para linkseo, mantiene el '/'
if(!function_exists('limpiar_url'))
{
	function limpiar_url($str, $replace=array(), $delimiter='-')
	{
		if (!empty($replace) )
		{
			$str = str_replace((array)$replace, ' ', $str);
		}

		/*setlocale(LC_CTYPE, 'es_PE.UTF-8');
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);*/
		$clean = convert_accented_characters($str);
		$clean = preg_replace("/[^a-zA-Z0-9_|+ \/-]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[_|+ -]+/", $delimiter, $clean);

		return $clean;
	}
}

function limpiar_detalle($detalle)
{
	return str_replace('"', "'", word_limiter(trim(html_entity_decode(strip_tags($detalle))), 10, FALSE));
}

function convertir_fecha($fecha)
{
	$timestamp = strtotime($fecha);
	$day = date('d', $timestamp);
	$month = ucfirst(strftime('%B', $timestamp));
	$year = date('Y', $timestamp);
	$hour = date('H:i', $timestamp);

	return $day.' de '.$month.' de '.$year.' '.$hour;
}

function validar_foto($array, $nombre)
{
	if(!empty($array[0]))
	{
		if(!empty($array[0]->$nombre->ruta))
		{
			$ruta = $array[0]->$nombre->ruta;
			$ruta = str_replace('http://static.golperu.pe/', 'http://static.golperu.pe.s3.amazonaws.com/', $ruta);
			return $ruta;
		}

	}

}