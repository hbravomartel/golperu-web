<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	// Crea el path para cada portadas individual
	if(!function_exists('portadas_path'))
	{
		function portadas_path($nid)
		{
			$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
			return FCPATH . 'json/portadas/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
		}
	}

	// Json con las últimas portadas
	if(!function_exists('portadas_path_lista')) 
	{  
	  function portadas_path_lista() 
	  {
	    return FCPATH . 'json/portadas/lista.json';
	  }
	}

	// Json con portadas históricas
	if(!function_exists('portadas_path_all'))
	{
		function portadas_path_all()
		{
			return FCPATH .'json/portadas/nids.json';
		}
	}
