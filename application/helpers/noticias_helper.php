<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	// Crea el path para cada noticia individual
	if(!function_exists('noticias_path'))
	{
		function noticias_path($nid)
		{
			$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
			return FCPATH . 'json/noticias/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
		}
	}	

	// Json con las últimas noticias
	if(!function_exists('noticias_path_lista')) 
	{  
	  function noticias_path_lista($seccion) 
	  {
	    return FCPATH . 'json/noticias/'.$seccion.'_lista.json';
	  }
	}

		// Json con las últimas noticias
	if(!function_exists('noticias_path_all')) 
	{  
	  function noticias_path_all() 
	  {
	    return FCPATH . 'json/noticias/all.json';
	  }
	}

	function noticias_path_nids()
	{
		return FCPATH . 'json/noticias/nids.json';
	}


	if(!function_exists('seccion_path_nid'))
	{
		function seccion_path_nid($seccion)
		{
			return FCPATH.'json/noticias/'.$seccion.'/nids.json';
		}
	}


	if(!function_exists('categoria_path_nid'))
	{
		function categoria_path_nid($seccion, $categoria)
		{
			return FCPATH.'json/noticias/'.$seccion.'/'.$categoria.'/nids.json';
		}
	}

	if(!function_exists('categoria_path_info'))
	{
		function categoria_path_info($seccion, $categoria)
		{
			return FCPATH.'json/noticias/'.$seccion.'/'.$categoria.'/info.json';
		}
	}

	// Crea el path para cada noticia individual
	if(!function_exists('nota_path'))
	{
		function nota_path($seccion, $nid)
		{			
			$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
			return FCPATH . 'json/'.$seccion.'/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
		}
	}

	function getMenuSeccion($seccion_id, $seccion_title)
	{
		$nav = '';

		switch ($seccion_id) {
			case 11:
				$nav = '<section class="seccion-head">
					        <div class="container">
					          <h2>'.$seccion_title.'</h2>
					          <div class="list">
					            <ul>
					              <li><a href="/copa-movistar/noticias">Noticias </a></li>
					              <li><a href="/copa-movistar/goles-de-la-fecha">Goles</a></li>
					              <li><a href="/copa-movistar/estadistica/">Estadísticas</a></li>
                        			<li><a href="/copa-movistar/fixture/">Fixture</a></li>
					            </ul>
					          </div>
					        </div>
					      </section>';
				break;

			case 12:
				$seccion_title = 'Fútbol Local';
				$nav = '<section class="seccion-head">
					        <div class="container">
					          <h2>'.$seccion_title.'</h2>
					          <div class="list">
					            <ul>
					              <li><a href="/futbol-local/torneo-de-promocion-y-reserva">Torneo de Promoción y Reserva</a></li>
					              <li><a href="/futbol-local/segunda-division">Segunda División</a></li>
					              <li><a href="/futbol-local/copa-peru">Copa Perú</a></li>
					            </ul>
					          </div>
					        </div>
					      </section>';
				break;

			case 13:
				$nav = '<section class="seccion-head">
					        <div class="container">
					          <h2>'.$seccion_title.'</h2>
					          <div class="list">
					            <ul>
					              	<li><a href="/ligas-internacionales/premier-league/">Premier League</a></li>
			                        <li><a href="/ligas-internacionales/serie-a/">Serie A</a></li>
			                        <li><a href="/ligas-internacionales/la-liga/">La Liga</a></li>
			                        <li><a href="/ligas-internacionales/bundesliga/">Bundesliga</a></li>
			                        <li><a href="/ligas-internacionales/otras-ligas-europa/">Otras Ligas Europa</a></li>
			                        <li><a href="/ligas-internacionales/otras-ligas-america/">Otras Ligas América</a></li>
			                        <li><a href="/ligas-internacionales/otras-ligas/">Otras Ligas</a></li>
					            </ul>
					          </div>
					        </div>
					      </section>';
				break;

			case 14:
				$nav = '<section class="seccion-head">
					        <div class="container">
					          <h2>'.$seccion_title.'</h2>
					          <div class="list">
					            <ul>
					              	<li><a href="/torneos-internacionales/copa-sudamericana/">Copa Sudamericana</a></li>
			                        <li><a href="/torneos-internacionales/copa-libertadores/">Copa Libertadores</a></li>
			                        <li><a href="/torneos-internacionales/champions-league/">UEFA Champions League</a></li>
			                        <li><a href="/torneos-internacionales/uefa/">UEFA Europa League</a></li>
					            </ul>
					          </div>
					        </div>
					      </section>';
				break;
			
			default:
				# code...
				break;
		}

		return $nav;

	}