<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


	if(!function_exists('validar_galeria'))
	{
		function validar_galeria($galeria, $key = NULL)
		{
			if (isset($key))
			{
				$galeria = json_decode($galeria);
				if(isset($galeria->$key) AND !empty($galeria->$key))
				{

					return $galeria->$key;
				}
				else
				{
					return FALSE;
				}
			}

			$array = json_decode($galeria);
			$properties = array();

			if(!empty($array))
			{
				foreach($array as $arr)
				{
					$properties = array_filter(get_object_vars($arr));
				}

				if(array_filter($properties))
				{
					return $array;
				}
			}


			return FALSE;
		}
	}

	if(!function_exists('tag_path_nid'))
	{
		function tag_path_nid($tag)
		{
			return FCPATH.'json/tags/'.$tag.'/nids.json';
		}

	}

	if(!function_exists('tag_path_nombre'))
	{
		function tag_path_nombre($tag)
		{
			return FCPATH.'json/tags/'.$tag.'/nombre.htm';
		}

	}

	function tag_path_elem($elem, $tag)
	{
		if(!is_dir(FCPATH.'json/tags/'.$tag.'/elementos'))
		{
			mkdir(FCPATH.'json/tags/'.$tag.'/elementos', 0755, TRUE);
		}
		return FCPATH.'json/tags/'.$tag.'/elementos/'.$elem.'.json';
	}

	function ruta_bio($tag)
	{
		return FCPATH.'json/tags/'.$tag.'/bio.htm';
	}

	function tag_get_elem($tag, $elem, $images = null)
	{

			if(is_file(FCPATH.'json/tags/'.$tag.'/'.$elem))
			{
				$url_content = file_get_contents(FCPATH.'json/tags/'.$tag.'/'.$elem);

				if(!empty($url_content))
				{
					return file_get_contents(FCPATH.'json/tags/'.$tag.'/'.$elem);
				}
			}
			if(!empty($tag))
			{
				$CI =& get_instance();
				$CI->load->helper('inflector');
				return humanize($tag, '-');
			}

			return FALSE;
	}

	function get_tags_nombres($tags)
	{
		$nombres = array();

		if(is_array($tags) && !empty($tags))
		{
			foreach($tags as $tag)
			{
				$nombres[] = tag_get_elem($tag, 'nombre.htm');
			}

			return $nombres;
		}
		return FALSE;
	}

	function get_tag_url($tag)
	{
		if(is_file(FCPATH.'json/tags/'.$tag.'/bio.htm'))
		{
			return 'artistas/'.$tag;
		}
		else
		{
			return 'tags/'.$tag;
		}
	}