        <div class="sidebar">

            <div class="mod-sidebar mod-positions tbl">
              <div class="head">
                <h3>Tabla de posiciones</h3>
              </div>
              <div class="cnt-body">
                <div class="tbl-responsive">
                  <opta-widget sport="football" widget="standings" competition="375" season="2017" template="normal" live="false" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="false" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="full" date_format="dddd D MMMM YYYY" sorting="false" show_live="false" show_relegation_average="false" show_logo="false" show_title="false" breakpoints="400" limit="10"></opta-widget>
                </div>
              </div>
            </div>
          
            <?php $this->load->view("ads/rectmedA") ?>
            

            <div class="mod-sidebar mod-goleadores">
              <div class="head">
                <h3>Goleadores</h3><span class="ico"></span>
              </div>
              <div class="cnt-body">                
                  <opta-widget sport="football" widget="player_ranking" competition="375" season="2017" template="normal" navigation="tabs_more" show_crests="false" show_images="false" show_ranks="true" show_appearances="true" visible_categories="goals" limit="10" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
               
              </div>
            </div>

            
            <div class="mod-sidebar mod-jugadores">
              <div class="head">
                <h3>Jugadores Destacados</h3>
              </div>
             <!--  <div class="cnt-body">
                <opta-widget sport="football" widget="player_ranking" competition="342" season="112018" team="834" template="normal" navigation="tabs_more" show_crests="false" show_images="false" show_ranks="true" show_appearances="true" visible_categories="goals" limit="10" hide_zeroes="false" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
                
              </div> -->
            </div>
            <div class="mod-sidebar mod-redes">
              <div class="head">
                <h3>Redes Sociales</h3>
              </div>
              <div class="cnt-body">
                <div class="cnt-tab">
                  <ul class="tab-redes">
                    <li><a data-tab="tab-fb" class="active">Facebook</a></li>
                    <li><a data-tab="tab-tw">Twitter</a></li>
                  </ul>
                </div>
                <div class="cnt-tab-content">
                  <div id="tab-fb" class="tab-content active"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FGOLPERU14%2F&tabs=timeline&width=300&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=260802547286772" width="300" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></div>
                  <div id="tab-tw" class="tab-content"><a class="twitter-timeline" href="https://twitter.com/GOLPERUoficial?ref_src=twsrc%5Etfw">Tweets by GOLPERUoficial</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
                </div>
              </div>
            </div>
            


            <?php $this->load->view("ads/rectmedB") ?>


          </div> <!-- fin sidebar -->