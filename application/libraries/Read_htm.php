<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Read_htm {

  private $CI;

  public function __construct()
  {
   $this->CI =&get_instance(); 
   $this->CI->load->library('motor');
  }

  public function noticia($nid, $part, $ext='htm')
  {
    return $this->get_($nid.$part, $this->CI->motor->noticia_path($nid), $ext);
  }

  public function encuesta($nid, $part, $ext='htm')
  {
    return $this->get_($nid.$part, $this->CI->motor->encuesta_path($nid), $ext);
  }

  public function jugador($nid, $part, $ext='htm')
  {
    return $this->get_($nid.$part, $this->CI->motor->jugador_path($nid), $ext);
  }

  public function categoria($nid, $part, $ext='htm')
  {
    return $this->get_($nid.$part, $this->CI->motor->categoria_path($nid), $ext);
  }


  public function get_($file, $ruta, $ext='htm')
  {
    $ruta.= "$file.$ext";
    
    if (file_exists($ruta))
    {
      return file_get_contents($ruta);
    }

    return NULL;
  }


}