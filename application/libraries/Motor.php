<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Motor {

	// Almacenamiento en físico
	function noticia_path($nid)
	{
		$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
		return FCPATH . 'json/noticias/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
	}

	function encuesta_path($nid)
	{
		$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
		return FCPATH . 'json/encuestas/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
	}

	function jugador_path($nid)
	{
		$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
		return FCPATH . 'json/jugadores/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
	}

	function categoria_path($nid)
	{
		$fname = str_pad($nid, 8, "0", STR_PAD_LEFT);
		return FCPATH . 'json/categorias/' . implode('/', preg_split('//', substr($fname, 0, strlen($fname)-3), -1, PREG_SPLIT_NO_EMPTY)) . '/' . $nid . '/';
	}		
}