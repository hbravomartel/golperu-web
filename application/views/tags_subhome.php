<?php $default = base_url().'assets/img/default-portada.jpg'; ?>
 
    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>

      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>

            <?php $this->load->view("includes/nav") ?>
          </div>
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      <?php if (!empty($nav_subhome))
              echo $nav_subhome;
              ?>


      <section class="principal">
        <div class="container cf nota">

          <?php $this->load->view("includes/sidebar") ?>

          
          <div class="content">
            <div class="wrap-nota">
              <h2 class="title"><?php echo $tag;?></h2>
              
            </div>
            <div class="section-inner">
              <div class="wrap-content">
                <div class="pagination clearfix">
                  <?php 
                    if(!empty($noticias)) : 
                      foreach($noticias as $noticia):?>    
                      <article class="flow flow-1x1 block_paginacion clearfix">
                        <figure class="flow-image">
                          <div class="gradient"></div>
                          <picture>
                            <source srcset="<?php echo !empty($noticia->fotomediano) ? $noticia->fotomediano : $default;?>"/><img src="<?php echo !empty($noticia->fotomediano) ? $noticia->fotomediano : $default;?>" alt="<?php echo $noticia->titular; ?>" />
                          </picture>
                        </figure>
                        <div class="detail">
                          <div class="flow-data">
                            <div class="date"> <i class="fa fa-clock-o"></i><?php echo $noticia->timestamp; ?></div>
                          </div>
                          <div class="title">
                            <h2><a href="<?php echo '/'.$noticia->linkseo.'-'.$noticia->nid;?>"><?php echo $noticia->titular; ?></a></h2>
                          </div>
                        </div>
                      </article>
                    <?php 
                      endforeach;
                    endif; ?>
                  
                    <div id="pag-subhome" class="pagination"></div> 
                  </div> 
              </div>
            </div>
            
            
          </div>
        </div>
      </section>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/pagination.js"></script>