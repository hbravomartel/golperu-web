<?php $default = base_url().'assets/img/default-portada.jpg'; ?>
 
    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>

      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>

            <?php $this->load->view("includes/nav") ?>
          </div>
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      <?php if (!empty($nav_subhome))
              echo $nav_subhome;
              ?>


      <section class="principal">
        <div class="container cf nota">

          <?php $this->load->view("includes/sidebar") ?>

          
          <div class="content">

              <div class="wrap-nota">              
                  <h2 class="title">Estadística</h2>             
               </div>

             <opta-widget sport="football" widget="standings" competition="375" season="2018" template="normal" live="true" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="true" lose_before_draw="false" show_form="6" competition_naming="full" team_naming="full" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="false" show_logo="false" show_title="false" breakpoints="400"></opta-widget>

      
          </div>
        </div>
      </section>