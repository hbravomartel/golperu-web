
<?php $default = base_url().'assets/img/default-portada.jpg'; ?>

    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>


      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>


            <?php $this->load->view("includes/nav") ?>
           
          </div> <!-- fin wrap-club-menu -->
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      



      <section class="principal clearfix">
        <div class="container cf clearfix">

          <?php $this->load->view("includes/sidebar") ?>
          

          <div class="content clearfix">
            <div class="section-inner">
              <div class="head">
                <h3 class="title">Buscador</h3><!-- <a href="#" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a> -->
              </div>
              <div class="wrap-content">
                <div class="pagination clearfix">

                 <script type="text/javascript">
                    function getParameterByName(name) {
                        url = window.location.href;
                        name = name.replace(/[\[\]]/g, "\\$&");
                        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                            results = regex.exec(url);
                        if (!results) return null;
                        if (!results[2]) return '';
                        return decodeURIComponent(results[2].replace(/\+/g, " "));
                    }
                  </script>
                  
                      <script>
                        //document.getElementById("btn_search").disabled = true;  
                        function UpdateCref(form) {
                          var q = form["q"];
                          var sa = form["sa"];
                          if (q.value == "") {
                            sa.disabled = true;
                          } else {
                          sa.disabled = false;
                          }
                        }

                        var param = getParameterByName("q");
                        console.log(param);
                        if(param)
                        {
                          $("#search").val(param);
                        }
                      </script>

                <!-- Resultados -->               
                <gcse:searchresults-only></gcse:searchresults-only>

                </div>           
              </div>
            </div>
          </div>
        </div>
      </section>




