
<?php $default = base_url().'assets/img/default-portada.jpg'; ?>
 
    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>

      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>

            <?php $this->load->view("includes/nav") ?>
          </div>
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      <?php if (!empty($nav_subhome))
              echo $nav_subhome;
              ?>


      <section class="principal">
        <div class="container cf nota">

          <?php $this->load->view("includes/sidebar") ?>

          
          <div class="content">
            <div class="wrap-nota">
              <div class="date"><?php echo convertir_fecha($noticia->timestamp);?></div>
              <h2 class="title"><?php echo $noticia->titular;?></h2>
              <div class="media-content">
                  <?php $this->load->view('includes/slider-noticia'); ?>              
              </div>
              <div class="text">
                <?php echo $noticia->desarrollo;?>
              </div>
              <!-- <div class="media-content">
                <div class="video">
                  <iframe width="100%" height="450" src="https://www.youtube.com/embed/z5GKYghDvec" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen="allowfullscreen"></iframe>
                </div>
              </div> -->
              <?php $this->load->view('includes/compartir'); ?>
            </div>
            <div class="section-inner">
              <div class="head">
                <h3 class="title">Noticia Relacionada</h3><!-- <a href="/<?php echo $nav_seccion; ?>/" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a> -->
              </div>
              <div class="wrap-content">
                <?php 
                  if(!empty($relacionadas)) : 
                    foreach($relacionadas as $nota_relacionada):?>    
                    <article class="flow flow-1x1">
                      <figure class="flow-image">
                        <div class="gradient"></div>
                        <picture>
                          <source srcset="<?php echo !empty($nota_relacionada->fotointerna) ? $nota_relacionada->fotointerna : $default;?>"/><img src="img/img_slider.png" alt="<?php echo $nota_relacionada->titular; ?>" />
                        </picture>
                      </figure>
                      <div class="detail">
                        <div class="flow-data">
                          <div class="date"> <i class="fa fa-clock-o"></i><?php echo $nota_relacionada->timestamp; ?></div>
                        </div>
                        <div class="title">
                          <h2><a href="<?php echo '/'.$nota_relacionada->linkseo.'-'.$nota_relacionada->nid;?>"><?php echo $nota_relacionada->titular; ?></a></h2>
                        </div>
                      </div>
                    </article>
                  <?php 
                    endforeach;
                  endif; ?>
                
              </div>
            </div>
            <div class="section-inner">
              <div class="head">
                <h3 class="title">Tags</h3><!-- <a href="#" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a> -->
              </div>
              <div class="wrap-content">
                <ul class="tags">
                  <?php if(!empty($tags)):?>
                    <div class="cont-tag margin-bottom padding-movil">
                      <?php foreach($tags as $key => $tag):?>
                        <li><a href="<?php echo '/'.$tag;?>"># <?php echo stripslashes($key);?></a></li>
                      <?php endforeach; ?>
                    </div>
                  <?php endif; ?>
                </ul>
              </div>
            </div>
            <!-- <div class="section-inner">
              <div class="head">
                <h3 class="title">Lo Más Leído</h3><a href="#" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a>
              </div>
              <div class="wrap-content wrap-content-masleido">
                <div class="slider-min-slick">
                  <div class="slider-item">
                    <article class="flow flow-min">         
                      <div class="num-l"># 1</div>
                      <div class="detail">
                        <div class="flow-data">
                          <div class="view"> <i class="fa fa-eye"></i>10,251</div>
                        </div>
                        <div class="title">
                          <h2><a>Primera División de Futsal: Panta Walon y Primero de Mayo son los</a></h2>
                        </div>
                      </div>
                      <figure class="flow-image">
                        <div class="gradient"></div>
                        <picture>
                          <source srcset="<?php echo base_url(); ?>assets/img/img_slider_min.png"/><img src="<?php echo base_url(); ?>assets/img/img_slider_min.png"/>
                        </picture>
                      </figure>
                    </article>
                  </div>
                  <div class="slider-item">
                    <article class="flow flow-min">         
                      <div class="num-l"># 1</div>
                      <div class="detail">
                        <div class="flow-data">
                          <div class="view"> <i class="fa fa-eye"></i>10,251</div>
                        </div>
                        <div class="title">
                          <h2><a>Primera División de Futsal: Panta Walon y Primero de Mayo son los</a></h2>
                        </div>
                      </div>
                      <figure class="flow-image">
                        <div class="gradient"></div>
                        <picture>
                          <source srcset="<?php echo base_url(); ?>assets/img/img_slider_min.png"/><img src="<?php echo base_url(); ?>assets/img/img_slider_min.png"/>
                        </picture>
                      </figure>
                    </article>
                  </div>
                  <div class="slider-item">
                    <article class="flow flow-min">         
                      <div class="num-l"># 1</div>
                      <div class="detail">
                        <div class="flow-data">
                          <div class="view"> <i class="fa fa-eye"></i>10,251</div>
                        </div>
                        <div class="title">
                          <h2><a>Primera División de Futsal: Panta Walon y Primero de Mayo son los</a></h2>
                        </div>
                      </div>
                      <figure class="flow-image">
                        <div class="gradient"></div>
                        <picture>
                          <source srcset="<?php echo base_url(); ?>assets/img/img_slider_min.png"/><img src="<?php echo base_url(); ?>assets/img/img_slider_min.png"/>
                        </picture>
                      </figure>
                    </article>
                  </div>
                  <div class="slider-item">
                    <article class="flow flow-min">         
                      <div class="num-l"># 1</div>
                      <div class="detail">
                        <div class="flow-data">
                          <div class="view"> <i class="fa fa-eye"></i>10,251</div>
                        </div>
                        <div class="title">
                          <h2><a>Primera División de Futsal: Panta Walon y Primero de Mayo son los</a></h2>
                        </div>
                      </div>
                      <figure class="flow-image">
                        <div class="gradient"></div>
                        <picture>
                          <source srcset="<?php echo base_url(); ?>assets/img/img_slider_min.png"/><img src="<?php echo base_url(); ?>assets/img/img_slider_min.png"/>
                        </picture>
                      </figure>
                    </article>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </section>