        <div class="sidebar">

            <div class="mod-sidebar mod-positions tbl">
              

 <?php 

if ($this->uri->segment(1)=="rusia-2018") {
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Fixture Rusia 2018</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
   echo '<opta-widget widget="fixtures" competition="4" season="2017" template="normal" live="false" show_venue="true" match_status="all" grouping="date" show_grouping="true" navigation="dropdown" default_nav="1" start_on_current="true" sub_grouping="date" show_subgrouping="false" order_by="date_ascending" show_crests="true" date_format="dddd D MMMM YYYY" time_format="HH:mm" month_date_format="MMMM" competition_naming="full" team_naming="full" pre_match="true" show_live="true" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget></div></div>';
}
else
{
    switch ($this->uri->segment(2)) {
    case "premier-league":
  echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
         echo '<opta-widget widget="standings" competition="8" season="2017" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
        break;
    case "serie-a": 
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
        echo '<opta-widget widget="standings" competition="21" season="2017" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
        break;
    case "la-liga":  
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
        echo '<opta-widget widget="standings" competition="23" season="2017" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
        break;
    case "bundesliga": 
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
        echo '<opta-widget widget="standings" competition="22" season="2017" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
        break;
    case "copa-sudamericana": 
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
          echo '<opta-widget widget="standings" competition="369" season="2018" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
          break;
    case "copa-libertadores": 
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
          echo '<opta-widget widget="standings" competition="420" season="2018"  template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
          break;
    case "champions-league":
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
          echo '<opta-widget widget="standings" competition="5" season="2017" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
          break;
    case "uefa":
   echo  '<div class="head"><h3><a href="" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive">';
          echo '<opta-widget widget="standings" competition="6" season="2017" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>';
          break;
    
    case "torneo-de-promocion-y-reserva"/*,"segunda-division" ,"copa-peru"*/:
          echo '<img src="http://golperu.pe/assets/img/ARDEN-LAS-REDES_WEB_300-X-600.gif">'; break;
    case "segunda-division"/*,"segunda-division" ,"copa-peru"*/:
          echo '<img src="http://golperu.pe/assets/img/ARDEN-LAS-REDES_WEB_300-X-600.gif">'; break;
    case "copa-peru":
          echo '<img src="http://golperu.pe/assets/img/ARDEN-LAS-REDES_WEB_300-X-600.gif">'; break;
    case "otras-ligas-europa":
          echo '<img src="http://golperu.pe/assets/img/ARDEN-LAS-REDES_WEB_300-X-600.gif">'; break;
    case "otras-ligas-america":
          echo '<img src="http://golperu.pe/assets/img/ARDEN-LAS-REDES_WEB_300-X-600.gif">'; break;
    case "otras-ligas":
          echo '<img src="http://golperu.pe/assets/img/ARDEN-LAS-REDES_WEB_300-X-600.gif">'; break;
     default:  echo  '<div class="head"><h3><a href="/copa-movistar/estadistica" style="color: #fff;">Tabla de posiciones</a></h3></div><div class="cnt-body"><div class="tbl-responsive"><opta-widget widget="standings" competition="375" season="2018" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget></div></div>'; break;
    }
}
?>
                  
                
            </div>
          
            <?php $this->load->view("ads/rectmedA") ?>
            

            <?php

if ($this->uri->segment(1)=="rusia-2018") {
    echo '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"> </div></div>';
}
else
{
   switch ($this->uri->segment(2)) {
    case "premier-league":  
              echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="8" season="2017" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "serie-a":        
             echo  '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="21" season="2017" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "la-liga":       
              echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="23" season="2017" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "bundesliga": 
             echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="22" season="2017"  template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "copa-sudamericana":  
              echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="369" season="2018" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "copa-libertadores": 
             echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="420" season="2018" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "champions-league": 
              echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="5" season="2017" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "uefa":  
             echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3>Goleadores</h3></div><div class="cnt-body"><opta-widget sport="football" widget="player_ranking" competition="6" season="2017" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;  
    case "torneo-de-promocion-y-reserva":  
              echo '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"></div></div>'; break;  
    case "copa-peru": 
             echo '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"></div></div>'; break;  
    case "otras-ligas-europa": 
             echo '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"></div></div>'; break;  
     case "otras-ligas-america": 
             echo '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"></div></div>'; break;  
     case "otras-ligas": 
             echo '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"></div></div>'; break;  
    case "segunda-division":  
              echo '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"></div></div>'; break;
    default: echo '<div class="mod-sidebar mod-goleadores"><div class="head"><h3><a href="/copa-movistar/estadistica" style="color: #fff;">Goleadores</a></h3></div><div class="cnt-body"><opta-widget widget="player_ranking" competition="375" season="2018" template="normal" navigation="tabs_more" show_crests="true" show_images="false" show_ranks="false" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="" show_title="false" breakpoints="400" sport="football"></opta-widget></div></div>'; break;
}
}
     ?>  

            
           <?php //$this->load->view("includes/jugadores") ?>

            
            <div class="mod-sidebar mod-redes">
              <div class="head">
                <h3>Redes Sociales</h3>
              </div>
              <div class="cnt-body">
                <div class="cnt-tab">
                  <ul class="tab-redes">
                    <li><a data-tab="tab-fb" class="active">Facebook</a></li>
                    <li><a data-tab="tab-tw">Twitter</a></li>
                  </ul>
                </div>
                <div class="cnt-tab-content">
                  <div id="tab-fb" class="tab-content active"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FGOLPERU14%2F&tabs=timeline&width=300&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=260802547286772" width="300" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></div>
                  <div id="tab-tw" class="tab-content"><a class="twitter-timeline" href="https://twitter.com/GOLPERUoficial?ref_src=twsrc%5Etfw">Tweets by GOLPERUoficial</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
                </div>
              </div>
            </div>
            


            <?php $this->load->view("ads/rectmedB") ?>


          </div> <!-- fin sidebar -->