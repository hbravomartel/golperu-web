      <section class="ticker">
        <div class="container cf">
          <div class="w-select">
            <select id="ticker">                  
              
              <option value="15">Fecha 15</option>
              <option value="14">Fecha 14</option>
              <option value="13">Fecha 13</option>
              <option value="12">Fecha 12</option>
              <option value="11">Fecha 11</option>
              <option value="10">Fecha 10</option>
              <option value="9">Fecha 9</option>
              <option value="8">Fecha 8</option>
              <option value="7">Fecha 7</option>
              <option value="6">Fecha 6</option>
              <option value="5">Fecha 5</option>
              <option value="4">Fecha 4</option>
              <option value="3">Fecha 3</option>
              <option value="2">Fecha 2</option>
              <option value="1" selected>Fecha 1</option>
              <!-- <option value="4">Fecha 5</option>
              <option value="4">Fecha 6</option> -->

            </select>
          </div>
          <div class="w-ticker" style="clear: inherit;"> 
            <div id="jornada-1" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="true" date_from="2018-02-03" date_to="2018-02-05" match_status="all" start_on_current="false" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget></div>

            <div id="jornada-2" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-02-09" date_to="2018-02-12" match_status="all" start_on_current="false" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget></div>

            <div id="jornada-3" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-02-17" date_to="2018-02-18" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-4" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-02-25" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-5" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-02-27" date_to="2018-03-01" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-6" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-03-02" date_to="2018-03-05" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-7" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-03-09" date_to="2018-03-12" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-8" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-03-13" date_to="2018-03-15" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-9" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-03-30" date_to="2018-04-02" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-10" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-04-06" date_to="2018-04-09" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-11" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-04-10" date_to="2018-04-12" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-12" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-04-13" date_to="2018-04-16" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-13" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-04-20" date_to="2018-04-23" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>

            <div id="jornada-14" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-04-29"  match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>
            </div>
            
            <div id="jornada-15" class="jornadas">
              <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="strip" live="false" date_from="2018-05-18" date_to="2018-05-24" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="abbr" team_naming="abbr" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget></div>
              

            

          </div>
        </div>
      </section>
