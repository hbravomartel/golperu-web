<style>
.result_graph{
  font-weight:bold;
}
.vote_graph{
  color:blue;
}
</style>
<div class="wrap-content wrap-content-encuesta cf" id="content-encuesta">

  <div class="encuesta-l">
    <div class="title">
   <!--  <?php //var_dump($encuesta); ?> -->

      <h2><?php echo $encuesta->encuesta_pregunta; ?></h2>
    </div>
    <form id="form_encuesta" name="form_encuesta" method="post">
      <input type="hidden" name="encuesta" id="encuesta" value="<?php echo $encuesta->encuesta_id; ?>">
      <div class="encuesta">

        <?php foreach ($encuesta->opciones as $opcion) { ?>
          <dl class="bloque_opcion">
          
          <dd> 
            <label class="check">
              <input type="radio" name="opcion" id="opcion" value="<?php echo $opcion->opcion_id; ?>" /><?php echo $opcion->opcion_nombre; ?>
            </label>
          </dd>
        </dl>
        <?php }?>

        <div class="w-btn">
          <button type="button" class="btn btn-primary btn_votar">Enviar<i class="fa fa-long-arrow-right"> </i></button>
        </div>
      </div>
    </form>
  </div>
  <div class="encuesta-r">
    <div id="encuesta_elemento" class="pie_result"></div>
  </div>

  <div class="col-md-12">
    <span class="msg_alerta" style="display:none">Debes seleccionar una opción para votar</span>
    <span class="msg_limite" style="display:none">Solo puedes votar 1 vez</span>
  </div>

</div>

<?php
  $json = 'json/encuestas/encuesta.json';
  $resultados = file_get_contents($json);
  $resultados = json_decode($resultados, true);

  

  $data = '';
  $x = 1;
  $cantidad = count($resultados);
  foreach ($resultados as $resultado) {
  if($x < $cantidad)
  {
    $data .= '{x: \'\n'.$resultado['opcion_nombre'].'\n'.'\', votos: '.$resultado['total'].'},';
  }   
  else
  {
    $data .= '{x: \'\n'.$resultado['opcion_nombre'].'\', votos: '.$resultado['total'].'}';
  } 
    
    $x++;
  }

  ?>
<script>
  Morris.Bar({
      element: 'encuesta_elemento',
      data: [<?php echo $data; ?>],
      xkey: 'x',
      ykeys: ['votos'],
      labels: ['Votos'],
      hideHover: 'auto',
      resize: true,
      gridTextSize: 9,
      xLabelAngle: 95,
      hoverCallback: function (index, options, content) {
        var votos = options.data[index].votos;
        var name = options.data[index].x;		  
        var result = name.replace("<p>",/ /g, "</p>");
        return '<div class="result_graph">'+result+'</div><div class="vote_graph">Votos: '+votos+'</div>';
      },
      
  });

  var tyc = $(".check");
  var alerta = $('.msg_alerta');

  $('.btn_votar').click(function(e){
      
      var form = document.form_encuesta;
      var dataString = $(form).serialize();
      var validacion = valEncuesta();

      console.log("encuesta","<br>");

      if(validacion){
          $.ajax({
              type: 'POST',
              url: '/encuestas/votar',
              data: dataString,
              dataType: 'json',
              success: function(data){
                
                
                if(data === 1)
                {               
                  $('#content-encuesta').load('encuestas/actualizar_encuesta');
                }
                else
                {
                  $('input[type="radio"]:not(:checked)');
                  $('.msg_limite').show();
                }

              },error: function(msg){
              console.log(msg);
              }
          });
      }
      else
      {
        $('.msg_alerta').show();
      }
  });

function valEncuesta()
{
    var cantidad = 0;

    $("input[name*='opcion']").each(function(){

        if($(this).is(':checked'))
        {
            cantidad++;
        }
        
    });

    if(cantidad > 0)
    {
      return true;
    }
    else
    {
      return false;
    }

}
</script>