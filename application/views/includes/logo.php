<div class="wrap-logo">
	<a href="<?php echo base_url(); ?>"><h1 class="logo"><img src="<?php echo base_url(); ?>assets/img/logo_golperu.png" class="hvr-pulse"/></h1></a>
	<div class="cnt-social">
	  <ul>
	    <li><a href="https://www.facebook.com/GOLPERU14/" target="_blank"> <i class="fa fa-facebook-square hvr-bounce-in"></i></a></li>
	    <li><a href="https://twitter.com/GOLPERUoficial" target="_blank"> <i class="fa fa-twitter hvr-bounce-in"></i></a></li>
	    <li><a href="https://www.youtube.com/channel/UCawRLLRENv1SOeGERA2_cWA"  target="_blank"> <i class="fa fa-youtube-play hvr-bounce-in" target="_blank"></i></a></li>
	  </ul>
	</div>
</div>