<?php
$seccion = $this->uri->segment(1); ?>
            <nav class="menu">
              <div class="inner-menu">
                <ul>
                  <li class="nav_logo">
                    <a href="<?php echo base_url(); ?>">
                      <h1 class="logo"><img src="<?php echo base_url(); ?>assets/img/logo_golperu_2019.png" class="hvr-pulse"/></h1>
                    </a>
                  </li>
                  <li><a href="#" class="<?php echo $seccion == "copa-movistar" ? "nav-active" : "";?>">Copa Movistar<i class="fa fa-angle-down"></i></a>
                    <div class="submenu">
                      <ul class="w100">
                        <li><a href="<?php echo base_url(); ?>copa-movistar/noticias/">Noticias</a></li>
                        <li><a href="<?php echo base_url(); ?>copa-movistar/goles-de-la-fecha/">Goles</a></li>
                      </ul>
                      <ul class="w100">
                        <li><a href="<?php echo base_url(); ?>copa-movistar/estadistica/">Estadísticas</a></li>
                        <li><a href="<?php echo base_url(); ?>copa-movistar/fixture/">Fixture</a></li>
                      </ul>
                    </div>
                  </li>
                  <li><a href="#" class="<?php echo $seccion == "futbol-local" ? "nav-active" : "";?>">Fútbol Local<i class="fa fa-angle-down"></i></a>
                    <div class="submenu">
                      <ul class="w100">
                       <!--  <li><a href="<?php echo base_url(); ?>futbol-local/futsal/">Futsal</a></li> -->
                        <li><a href="<?php echo base_url(); ?>futbol-local/torneo-de-promocion-y-reserva/">Torneo de Promoción y Reserva</a></li>
                        <li><a href="<?php echo base_url(); ?>futbol-local/segunda-division/">Segunda División</a></li>
                        <li><a href="<?php echo base_url(); ?>futbol-local/copa-peru/">Copa Perú</a></li>
                      </ul>
                    </div>
                  </li>
                  <li><a href="#" class="<?php echo $seccion == "ligas-internacionales" ? "nav-active" : "";?>">Ligas Internacionales<i class="fa fa-angle-down"></i></a>
                    <div class="submenu">
                      <ul class="w100">
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/premier-league/">Premier League</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/serie-a/">Serie A</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/la-liga/">La Liga</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/bundesliga/">Bundesliga</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/otras-ligas-europa/">Otras Ligas Europa</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/otras-ligas-america/">Otras Ligas América</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/otras-ligas/">Otras Ligas</a></li>
                      </ul>
                    </div>
                  </li>
                  <li><a href="#" class="<?php echo $seccion == "torneos-internacionales" ? "nav-active" : "";?>">Torneos Internacionales<i class="fa fa-angle-down"></i></a>
                    <div class="submenu">
                      <ul class="w100">
                        <li><a href="<?php echo base_url(); ?>torneos-internacionales/copa-sudamericana/">Copa Sudamericana</a></li>
                        <li><a href="<?php echo base_url(); ?>torneos-internacionales/copa-libertadores/">Copa Libertadores</a></li>
                        <li><a href="<?php echo base_url(); ?>torneos-internacionales/champions-league/">UEFA Champions League</a></li>
                        <li><a href="<?php echo base_url(); ?>torneos-internacionales/uefa/">UEFA Europa League</a></li>
                      </ul>
                    </div>
                  </li>
                  <li><a href="<?php echo base_url(); ?>rusia-2018/">Selecciones</a></li>
                  <!-- <li><a href="/programacion/">Programación</a></li> -->
                </ul>
                <!-- <div class="search">
                  <form action="/buscador">
                    <div class="container-search"><span class="icon"><i class="fa fa-search"></i></span>
                      <input type="search" name="q" id="search" placeholder="Buscar..." class="search-text"/>
                    </div>
                  </form>
                </div>
                <button type="button" class="menu-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar">
                    </span></button> -->
              </div>
            </nav> <!-- fin menu -->