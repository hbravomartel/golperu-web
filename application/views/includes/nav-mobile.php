<section class="mobile-menu">
        <div class="container cf">
          <div class="menu-m">
            <ul>
              <li><a href="#">Copa Movistar<i class="fa fa-angle-down"></i></a>
                <div class="submenu">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>copa-movistar/noticias/">Noticias</a></li>
                        <li><a href="<?php echo base_url(); ?>copa-movistar/goles-de-la-fecha/">Goles de la fecha</a></li>
                  </ul>
                </div>
              </li>
              <li><a href="#">Fútbol Local<i class="fa fa-angle-down"></i></a>
                <div class="submenu">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>futbol-local/torneo-de-promocion-y-reserva/">Torneo de Promoción y Reserva</a></li>
                    <li><a href="<?php echo base_url(); ?>futbol-local/segunda-division/">Segunda División</a></li>
                    <li><a href="<?php echo base_url(); ?>futbol-local/copa-peru/">Copa Perú</a></li>
                  </ul>
                </div>
              </li>
              <li><a href="#">Ligas Internacionales<i class="fa fa-angle-down"></i></a>
                <div class="submenu">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>ligas-internacionales/premier-league/">Premier League</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/serie-a/">Serie A</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/la-liga/">La Liga</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/bundesliga/">Bundesliga</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/otras-ligas-europa/">Otras Ligas Europa</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/otras-ligas-america/">Otras Ligas América</a></li>
                        <li><a href="<?php echo base_url(); ?>ligas-internacionales/otras-ligas/">Otras Ligas</a></li>
                  </ul>
                </div>
              </li>
              <li><a href="#">Torneos Internacionales<i class="fa fa-angle-down"></i></a>
                <div class="submenu">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>torneos-internacionales/copa-sudamericana/">Copa Sudamericana</a></li>
                        <li><a href="<?php echo base_url(); ?>torneos-internacionales/copa-libertadores/">Copa Libertadores</a></li>
                        <li><a href="<?php echo base_url(); ?>torneos-internacionales/champions-league/">UEFA Champions League</a></li>
                        <li><a href="<?php echo base_url(); ?>torneos-internacionales/uefa/">UEFA Europa League</a></li>
                  </ul>
                </div>
              </li>
              <li><a href="<?php echo base_url(); ?>rusia-2018/">Selecciones </a>
                
              </li>
            </ul>
          </div>
        </div>
      </section>