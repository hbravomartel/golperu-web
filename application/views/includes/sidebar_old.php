        <div class="sidebar">

          <?php if(!empty($this->competition_id)) : ?>
            <div class="mod-sidebar mod-positions tbl">
              <div class="head">
                <h3>Tabla de posiciones</h3>
              </div>
              <div class="cnt-body">
                <div class="tbl-responsive">
                  <!-- <opta-widget sport="football" widget="standings" competition="375" season="2018" template="normal" live="true" default_nav="1" side="combined" data_detail="brief" show_key="false" show_crests="true" points_in_first_column="false" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="false" show_relegation_average="false" show_logo="false" show_title="false" breakpoints="400"></opta-widget> -->
                  <!-- <opta-widget widget="standings" competition="375" season="2018" template="normal" live="true" default_nav="1" side="home" data_detail="brief" show_key="false" show_crests="true" points_in_first_column="true" lose_before_draw="false" show_form="0" competition_naming="brief" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="false" show_logo="false" show_title="false" breakpoints="400" sport="football"></opta-widget> -->

                  
                    <opta-widget widget="standings" competition="<?php echo $this->competition_id; ?>" season="<?php echo $this->season;?>" template="normal" live="true" team_padding="0" default_nav="1" side="combined" data_detail="default" show_key="false" show_crests="true" points_in_first_column="true" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget>
                  

                  <!-- <opta-widget widget="standings" competition="375" season="2018" template="normal" live="false" team_padding="0" default_nav="1" side="away" data_detail="brief" show_key="false" show_crests="true" points_in_first_column="true" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="abbr" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="true" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget> -->
                  
                </div>
              </div>
            </div>
          <?php endif; ?>
          
            <?php $this->load->view("ads/rectmedA") ?>
            
            <?php if(!empty($this->competition_id)) : ?> 
              <div class="mod-sidebar mod-goleadores">
                <div class="head">
                  <h3>Goleadores</h3>
                </div>
                <div class="cnt-body">    
                             
                    <opta-widget widget="player_ranking" competition="<?php echo $this->competition_id; ?>" season="<?php echo $this->season;?>" template="normal" navigation="dropdown" show_crests="true" show_images="false" show_ranks="true" show_appearances="false" visible_categories="goals" limit="5" hide_zeroes="true" show_team_names="true" team_naming="abbr" player_naming="initial" show_logo="false" show_title="false" breakpoints="100" sport="football"></opta-widget>

                </div>
              </div>
            <?php endif; ?>
            
           <?php //$this->load->view("includes/jugadores") ?>

            
            <div class="mod-sidebar mod-redes">
              <div class="head">
                <h3>Redes Sociales</h3>
              </div>
              <div class="cnt-body">
                <div class="cnt-tab">
                  <ul class="tab-redes">
                    <li><a data-tab="tab-fb" class="active">Facebook</a></li>
                    <li><a data-tab="tab-tw">Twitter</a></li>
                  </ul>
                </div>
                <div class="cnt-tab-content">
                  <div id="tab-fb" class="tab-content active"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FGOLPERU14%2F&tabs=timeline&width=300&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=260802547286772" width="300" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></div>
                  <div id="tab-tw" class="tab-content"><a class="twitter-timeline" href="https://twitter.com/GOLPERUoficial?ref_src=twsrc%5Etfw">Tweets by GOLPERUoficial</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
                </div>
              </div>
            </div>
            


            <?php $this->load->view("ads/rectmedB") ?>


          </div> <!-- fin sidebar -->