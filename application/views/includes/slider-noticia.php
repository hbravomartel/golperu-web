<?php 
  $default_p = base_url().'assets/img/default-portada.jpg';

  // FOTO DEFAULT 
  if(empty($noticia->fotos) && empty($noticia->videos)){ ?>
  <div class="top_slick compartir_negro clearfix position_relative">
      <picture>
        <source srcset="<?php echo !empty($noticia->fotoportada) ? $noticia->fotoportada : $default_p;?>"/><img src="<?php echo !empty($noticia->fotoportada) ? $noticia->fotoportada : $default_p;?>" alt="<?php echo $noticia->titular;?>" />
      </picture>

  <?php } ?>

  <div class="slider slider-slick">
 
    <?php if(!empty($noticia->fotos)){ ?>
      <?php foreach($noticia->fotos as $foto): 
      $foto->apaisado->ruta = str_replace('http://static.golperu.pe/', 'http://static.golperu.pe.s3.amazonaws.com/', $foto->apaisado->ruta);
      ?>

                  <div class="slider-item">
                    <article class="flow">
                      <div class="gradient"></div>
                      <figure class="flow-image">
                        <picture>
                          <source srcset="<?php echo !empty($foto->apaisado->ruta) ? $foto->apaisado->ruta : $default_p;?>"/><img src="<?php echo !empty($foto->apaisado->ruta) ? $foto->apaisado->ruta : $default_p;?>" alt="<?php echo $noticia->titular;?>" />
                        </picture>
                      </figure>
                    </article>
                  </div>

      <?php endforeach; ?>      
    <?php } ?>

    <?php if(!empty($noticia->videos)){ ?>
      <?php foreach($noticia->videos as $video): ?>

        <div class="slider-item">
          <article class="flow">               
            <div class="flow-video">
              <div class="video">
                <?php $data['video'] = $video; $this->load->view('includes/iframe_videos', $data);?>
              </div>
            </div>
          </article>
        </div>

      <?php endforeach; ?>      
    <?php } ?>

</div>

