      <section class="ticker">
        <div class="container cf">
<?php  switch ($this->uri->segment(2)) {
            case "premier-league": 
                      $NombreCampeonato="Premier League";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="8" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
            case "serie-a": 
                     $NombreCampeonato="Serie A";         
                     $TickerCampeonato= '<opta-widget widget="fixtures" competition="21" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
            case "la-liga": 
                      $NombreCampeonato="La Liga";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="23" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
            case "bundesliga": 
                      $NombreCampeonato="Bundesliga";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="22" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
            case "copa-sudamericana": 
                      $NombreCampeonato="Copa Sudamericana";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="369" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
             case "copa-libertadores": 
                      $NombreCampeonato="Copa Libertadores";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="420" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
            case "champions-league":
                      $NombreCampeonato="Champions League";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="5" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
            case "uefa":  
                      $NombreCampeonato="Copa UEFA";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="6" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
            case "torneo-de-promocion-y-reserva":  
                      $NombreCampeonato="Torneo promoción y reserva";         
                      $TickerCampeonato= '<div class="mod-sidebar mod-goleadores"><div class="cnt-body"></div></div>'; break;  
            case "copa-peru": 
                     $NombreCampeonato="Copa Perú";         
                     $TickerCampeonato= '&nbsp;'; break;  
            case "otras-ligas-europa": 
                    $NombreCampeonato="Otras Ligas Europa";         
                     $TickerCampeonato= '&nbsp;'; break;  
             case "otras-ligas-america": 
                      $NombreCampeonato="Otras Ligas America";         
                     $TickerCampeonato= '&nbsp;'; break;  
             case "otras-ligas": 
                     $NombreCampeonato="Otras Ligas";         
                     $TickerCampeonato= '&nbsp;'; break;  
            case "segunda-division":  
                      $NombreCampeonato="Segunda división";         
                      $TickerCampeonato= '&nbsp;'; break;
            default:
                      $NombreCampeonato="Copa Movistar Torneo Clausura";         
                      $TickerCampeonato= '<opta-widget widget="fixtures" competition="375" season="2018" template="strip" live="false"  date_from="2018-08-30" date_to="2018-12-31" match_status="all" start_on_current="true" order_by="date_ascending" show_crests="true" show_date="true" date_format="DD/MM/YYYY" time_format="HH:mm" competition_naming="brief" team_naming="brief" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>'; break;
}?>

          <div class="w-select"> <br><b><h3 style="color: #0050b4;"><?=$NombreCampeonato; ?></h3></b></div>
          <div class="w-ticker" style="clear: inherit;"> 
            <?=$TickerCampeonato; ?>
          </div>
        </div>
      </section>
