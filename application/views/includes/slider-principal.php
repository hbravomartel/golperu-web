              <div class="slider slider-slick">
                <?php 
                  if(!empty($noticias)):
                    foreach($noticias as $noticia):?>
                      <div class="slider-item">                        
                          <article class="flow">               
                            <figure class="flow-image">
                              <div class="gradient"></div>
                              <picture>
                                <source srcset="<?php echo $noticia->fotoportada; ?>"/><img src="<?php echo !empty($noticia->fotoportada) ? $noticia->fotoportada : $default_portada;?>" alt="<?php echo $noticia->titular;?>"/>
                              </picture>
                            </figure>
                            <div class="detail">
                              <div class="flow-data">
                                <h3 class="category"><a><?php echo $noticia->categoria;?></a></h3>
                              </div>
                              <div class="title">
                                <h2><a href="/<?php echo $noticia->linkseo.'-'.$noticia->nid;?>"><?php echo $noticia->titular;?></a></h2>
                              </div>
                              <div class="extract">
                                <p><?php echo limpiar_detalle($noticia->desarrollo).'...';?></p>
                              </div>
                            </div>
                          </article>
                      </div>
                  <?php 
                    endforeach; 
                  endif;
                  ?>

              </div> <!-- fin slider -->