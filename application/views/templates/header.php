<?php 
if(!empty($meta_title))
$titulo = 'GOLPERU - '.$meta_title;

else
$titulo = 'GOLPERU';
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1"/>
    <title>GOLPERU</title>

    <meta name="description" content="<?php echo !empty($meta_description) ? $meta_description : DEFAULT_DESCRIPTION;?>">

    <link rel="image_src" href="<?php echo !empty($meta_image) ? $meta_image : base_url().'assets/img/logo_golperu.png' ;?>" id="image_src" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $titulo; ?>" />

    <?php if(!empty($meta_keywords)):?>
    <meta name="keywords" content="<?php echo $meta_keywords;?>">
    <meta name="news_keywords" content="<?php echo $meta_keywords;?>">
    <?php endif; ?>

    <meta property="og:url" content="https://moda.com.pe/<?php echo uri_string();?>" />
    <meta property="og:image" content="<?php echo !empty($meta_image) ? $meta_image : base_url().'assets/img/logo_golperu.png';?>" />
    <meta property="og:site_name" content="GOLPERU" />
    <meta property="og:description" content="<?php echo !empty($meta_description) ? $meta_description : DEFAULT_DESCRIPTION;?>" />
    <meta property="og:locale" content="es_ES" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@GOLPERU14">
    <meta name="twitter:title" content="<?php echo $titulo; ?>">
    <meta name="twitter:description" content="<?php echo !empty($meta_description) ? $meta_description : DEFAULT_DESCRIPTION;?>">
    <meta name="twitter:image" content="<?php echo !empty($meta_image) ? $meta_image : base_url().'assets/img/logo_golperu.png';?>">


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick-theme.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ;?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url() ;?>assets/css/hover-min.css">
    <style>
        .Opta .Opta-Scroll .Opta-Scroller{
            background-color: #0050b4;
        }
        .Opta .Opta-Scroll .Opta-Scroller.Opta-Active{
            background-color: #0050b4;
        }
        
        .Opta .Opta-Scroll .Opta-Scroller.Opta-Active:hover{
            background-color: #e3c63c;
        }
        #encuesta_elemento{
            height: 430px;
            padding-top: 32px;
            margin-bottom: 25px;
        }
        
        .menu{
            height: 64px;
        }
        .menu ul li .submenu{
            border-style: ridge;
            top:38px;
        }
        #pag-subhome{
            padding-left: 23%;
        }
        .light-theme .current{
            background: #0050b4;
            border-color: #0050b4;
        }
        .menu .inner-menu ul li a:hover, .menu .inner-menu ul li a:focus{
            background: rgba(0,0,0,0.1);
        }
        
        .ir-arriba {
                display:none;
                padding:15px;
                background:#0050b4;
                font-size:15px;
                color:#fff;
                cursor:pointer;
                position: fixed;
                bottom:0px;
                right:20px;
                border-radius: 2px;
                z-index: 1;
        }
        
        .ir-arriba:hover {
            background: #e3c63c;
        }
    </style>
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo_golperu1.png"/>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <!-- OPTA -->
    <script src="http://widget.cloud.opta.net/v3/v3.opta-widgets.js"></script>
    <link rel="stylesheet" href="http://widget.cloud.opta.net/v3/css/v3.football.opta-widgets.css">
    <script>
        var opta_settings = {
                subscription_id: '3005df5a4344dc9c28a2d0bbeca233e5',
                language: 'es_CO',
                timezone: 'America/Lima'
            };
    </script>
    <script src="<?php echo base_url(); ?>assets/js/ticker.js?v=1"></script>

        <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-92981502-19', 'auto');
    ga('create', 'UA-92981502-2', 'auto', 'allDeployentsTracker');
    ga('create', 'UA-53523022-18', 'auto', 'clientTracker');

    ga('send', 'pageview', window.location.pathname);
    ga('allDeployentsTracker.send','pageview', window.location.pathname);
    ga('clientTracker.send','pageview', window.location.pathname);

    </script>
    <script>
        $(function(){
           $('.ir-arriba').click(function(){
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    });
 
    $(window).scroll(function(){
        if( $(this).scrollTop() > 500 ){
            $('.ir-arriba').slideDown(300);
        } else {
            $('.ir-arriba').slideUp(300);
        }
    }); 
        });
    </script>

    <!-- PAGINACION -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.simplePagination.js"></script>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/css/simplePagination.css"/>

    <!-- OPTA-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/opta.css"/>
    <!--- Configuracion Publicidad -->
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/27185205/GOLPERU-CAJA', [300, 250], 'div-gpt-ad-1524583615344-0').addService(googletag.pubads());
    googletag.defineSlot('/27185205/GOLPERU-CAJAGRANDE', [300, 600], 'div-gpt-ad-1524583615344-1').addService(googletag.pubads());
    googletag.defineSlot('/27185205/GOLPERU-TOP', [970, 90], 'div-gpt-ad-1524583615344-2').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>


    <!--- Fin Publicidad  -->

  </head>
  <body class="home">  