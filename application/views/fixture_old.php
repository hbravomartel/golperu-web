<?php $default = base_url().'assets/img/default-portada.jpg'; ?>
 
    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>

      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>

            <?php $this->load->view("includes/nav") ?>
          </div>
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      <?php if (!empty($nav_subhome))
              echo $nav_subhome;
              ?>


      <section class="principal">
        <div class="container cf nota">

          <?php $this->load->view("includes/sidebar") ?>

          
          <div class="content">
              <div class="wrap-nota">              
                  <h2 class="title">Fixture</h2>             
               </div>

             <opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="normal" live="true" team_filter="home" show_venue="false" match_status="result" grouping="date" show_grouping="true" default_nav="1" start_on_current="false" sub_grouping="date" show_subgrouping="false" order_by="date_ascending" show_crests="true" date_format="dddd D MMMM YYYY" time_format="hh:mm A" month_date_format="MMMM" competition_naming="full" team_naming="full" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>

      
          </div>
        </div>
      </section>