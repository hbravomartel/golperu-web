
<?php $default = base_url().'assets/img/default-portada.jpg'; ?>

    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>


      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>


            <?php $this->load->view("includes/nav") ?>
           
          </div> <!-- fin wrap-club-menu -->
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      <?php if (!empty($nav_subhome))
              echo $nav_subhome;
              ?>



      <section class="featured">
        <div class="container cf"> 
          <div class="content">
            <div class="media-content">
              <div class="slider slider-slick">

                <?php 
                  if(!empty($noticias_slider)):
                    foreach($noticias_slider as $noticia_slider):?>
                      <div class="slider-item">                        
                          <article class="flow">               
                            <figure class="flow-image">
                              <div class="gradient"></div>
                              <picture>
                                <source srcset="<?php echo $noticia_slider->fotoportada; ?>"/><img src="<?php echo !empty($noticia_slider->fotoportada) ? $noticia_slider->fotoportada : $default;?>" alt="<?php echo $noticia_slider->titular;?>"/>
                              </picture>
                            </figure>
                            <div class="detail">
                              <div class="flow-data">
                                <h3 class="category"><a><?php echo $noticia_slider->categoria;?></a></h3>
                              </div>
                              <div class="title">
                                <h2><a href="/<?php echo $noticia_slider->linkseo.'-'.$noticia_slider->nid;?>"><?php echo $noticia_slider->titular;?></a></h2>
                              </div>
                              <div class="extract">
                                <p><?php echo limpiar_detalle($noticia_slider->desarrollo).'...';?></p>
                              </div>
                            </div>
                          </article>
                      </div>
                  <?php 
                    endforeach; 
                  endif;
                  ?>
                
              </div>
            </div>
          </div>
        </div>
      </section>



      <section class="principal">
        <div class="container cf">

          <?php $this->load->view("includes/sidebar") ?>

          <div class="content">
            <div class="section-inner">
              <div class="head">
                <h3 class="title"><?php echo $seccion; ?></h3><!-- <a href="#" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a> -->
              </div>
              <div class="wrap-content">
                <div class="pagination clearfix">

                 <?php 
                  if(!empty($noticias)):
                    foreach($noticias as $noticia):?>
                      <article class="flow flow-1x1 block_paginacion">
                          <figure class="flow-image">
                            <div class="gradient"></div>
                            <picture>
                              <source srcset="<?php echo !empty($noticia->fotointerna) ? $noticia->fotointerna : $default; ?>"/><img src="<?php echo !empty($noticia->fotointerna) ? $noticia->fotointerna : $default; ?>" alt="<?php echo $noticia->titular;?>"/>
                            </picture>
                          </figure>
                          <div class="detail">       
                            <div class="flow-data">
                              <div class="date"> <i class="fa fa-clock-o"></i><?php echo $noticia->timestamp;?></div>
                            </div>
                            <div class="title">
                              <h2>
                                <a href="/<?php echo $noticia->linkseo.'-'.$noticia->nid;?>">
                                  <?php echo $noticia->titular;?>
                                </a>
                              </h2>
                            </div>
                          </div>
                      </article>

              <?php endforeach; 
                  endif; 
                  ?> 

                  <div id="pag-subhome" class="pagination"></div>    
                </div>           
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/pagination.js?v=1"></script>



