<?php $default = base_url().'assets/img/default-portada.jpg'; ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>   
<script src="<?php echo base_url(); ?>assets/js/morris.js"></script> 

    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>

      <header id="header" class="full-width">
        <div class="container cf">

          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">  

            <?php $this->load->view("includes/clubs") ?>

            <?php $this->load->view("includes/nav") ?>

          </div> <!-- fin wrap-club-menu -->
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      

      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>

   

      <section class="featured">
        <div class="container cf"> 
          <div class="content">
            <div class="media-content">

              <?php $this->load->view("includes/slider-principal") ?>
              
            </div>
          </div>
        </div>
      </section>
      <section class="principal">
        <div class="container cf">
            <span class="ir-arriba icon-circle-up btn-primary"></span>

          <?php $this->load->view("includes/sidebar") ?>

          <div class="content">

            <div class="section-inner">
              <div class="head">
                <h3 class="title">Rusia 2018</h3><a href="/rusia-2018" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a>
              </div>
              <div class="wrap-content">

                <?php 
                  if(!empty($noticias_mundial)):
                    foreach($noticias_mundial as $noticia_rusa):?>
                      <article class="flow flow-1x1">
                        
                          <div class="badge"> <a class="blue">Rusia 2018</a></div>
                          <figure class="flow-image hvr-grow">
                            <div class="gradient"></div>
                            <picture>
                              <source srcset="<?php echo !empty($noticia_rusa->fotointerna) ? $noticia_rusa->fotointerna : $default; ?>"/><img src="<?php echo !empty($noticia_rusa->fotointerna) ? $noticia_rusa->fotointerna : $default; ?>" alt="<?php echo $noticia_rusa->titular;?>"/>
                            </picture>
                          </figure>
                          <div class="detail">       
                            <div class="flow-data">
                              <div class="date"> <i class="fa fa-clock-o"></i><?php echo $noticia_rusa->timestamp;?></div>
                            </div>
                            <div class="title">
                              <h2>
                                <a href="/<?php echo $noticia_rusa->linkseo.'-'.$noticia_rusa->nid;?>">
                                  <?php echo $noticia_rusa->titular;?>
                                </a>
                              </h2>
                            </div>
                          </div>
                      </article>

              <?php endforeach; 
                  endif; 
                  ?>                         
              </div>
            </div>

            <div class="section-inner">
              <div class="head">
                <h3 class="title">Copa Movistar</h3><a href="copa-movistar/" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a>
              </div>
              <div class="wrap-content-envivo">
                <article class="flow hvr-bob">
                  <figure class="flow-image">
                    <div class="gradient"></div>
                    <picture>
                      <source srcset="<?php echo !empty($noticias_copa_slider[0]->fotoportada) ? $noticias_copa_slider[0]->fotoportada : $default; ?>"/><img src="<?php echo !empty($noticias_copa_slider[0]->fotoportada) ? $noticias_copa_slider[0]->fotoportada : $default; ?>"/>
                    </picture>
                  </figure>
                  <div class="detail">       
                    <div class="flow-data">
                      <div class="date"> <i class="fa fa-clock-o"></i><?php echo convertir_fecha($noticias_copa_slider[0]->timestamp); ?></div>
                    </div>
                    <div class="title">
                      <h2><a href="/<?php echo $noticias_copa_slider[0]->linkseo.'-'.$noticias_copa_slider[0]->nid;?>"><?php echo $noticias_copa_slider[0]->titular; ?></a></h2>
                    </div>

                  </div>
                </article>
              </div>
              <div class="wrap-content w-flow-1">

              <?php 
                  if(!empty($noticias_copa)):
                    foreach($noticias_copa as $noticia_copa):?>
                      <article class="flow flow-1x1">
                        
                          <div class="flow-mediatype">
                            <span class="ico-image"></span>
                          </div>
                          <figure class="flow-image hvr-grow">
                            <div class="gradient"></div>
                            <picture>
                              <source srcset="<?php echo !empty($noticia_copa->fotointerna) ? $noticia_copa->fotointerna : $default; ?>"/><img src="<?php echo !empty($noticia_copa->fotointerna) ? $noticia_copa->fotointerna : $default; ?>" alt="<?php echo $noticia_copa->titular;?>"/>
                            </picture>
                          </figure>
                          <div class="detail">       
                            <div class="flow-data">
                              <div class="date"> <i class="fa fa-clock-o"></i><?php echo $noticia_copa->timestamp;?></div>
                            </div>
                            <div class="title">
                              <h2>
                                <a href="/<?php echo $noticia_copa->linkseo.'-'.$noticia_copa->nid;?>">
                                  <?php echo $noticia_copa->titular;?>
                                </a>
                              </h2>
                            </div>
                          </div>
                      </article>

              <?php endforeach; 
                  endif; 
                  ?>
                
                
              </div>
            </div>
            
            <div class="section-inner">
              <div class="head">
                <h3 class="title">#ArdenLasRedes</h3>
              </div>

              <?php $this->load->view("includes/encuesta"); ?>
              
            </div>
            <div class="section-inner">
              <div class="head">
                <h3 class="title">Ligas Internacionales</h3><a href="ligas-internacionales/" class="lnk">Ver todo<i class="fa fa-angle-right"></i></a>
              </div>
              <div class="wrap-content">

              <?php 
                  if(!empty($noticias_ligas)):
                    foreach($noticias_ligas as $noticia_liga):?>
                      <article class="flow flow-1x1">
                        
                          <div class="badge"> <a class="blue"><?php echo $noticia_liga->categoria; ?></a></div>
                          <figure class="flow-image hvr-grow">
                            <div class="gradient"></div>
                            <picture>
                              <source srcset="<?php echo !empty($noticia_liga->fotointerna) ? $noticia_liga->fotointerna : $default; ?>"/><img src="<?php echo !empty($noticia_liga->fotointerna) ? $noticia_liga->fotointerna : $default; ?>" alt="<?php echo $noticia_liga->titular;?>"/>
                            </picture>
                          </figure>
                          <div class="detail">       
                            <div class="flow-data">
                              <div class="date"> <i class="fa fa-clock-o"></i><?php echo $noticia_liga->timestamp;?></div>
                            </div>
                            <div class="title">
                              <h2>
                                <a href="/<?php echo $noticia_liga->linkseo.'-'.$noticia_liga->nid;?>">
                                  <?php echo $noticia_liga->titular;?>
                                  </a>
                              </h2>
                            </div>
                          </div>
                      </article>

              <?php endforeach; 
                  endif; 
                  ?>                
              </div>
            </div>
            
          </div>
        </div>
      </section>



