<?php $default = base_url().'assets/img/default-portada.jpg'; ?>
 
    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>

      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>

            <?php $this->load->view("includes/nav") ?>
          </div>
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      <?php if (!empty($nav_subhome))
              echo $nav_subhome;
              ?>
      <?php if ($this->uri->segment(2)=="estadistica")
          
   echo' <section class="seccion-head">';
echo'        <div class="container">';
echo'          <h2>Copa Movistar</h2>';
echo'          <div class="list">';
echo'            <ul>';
echo'              <li><a href="/copa-movistar/noticias">Noticias </a></li>';
echo'              <li><a href="/copa-movistar/goles-de-la-fecha">Goles </a></li>';
echo'              <li><a href="/copa-movistar/estadistica/">Estadísticas</a></li>';
echo'                    <li><a href="/copa-movistar/fixture/">Fixture</a></li>';
echo'            </ul>';
echo'          </div>';
echo'        </div>';
echo'      </section>';

      
      ?>

      <section class="principal">
        <div class="container cf nota">

          <?php $this->load->view("includes/sidebar") ?>

          
          <div class="content">

              <div class="wrap-nota">              
                  <h2 class="title">Estadística</h2>             
               </div>


              <div class="cnt-body">
                <div class="cnt-tab">
                  <ul class="tab-redes">
                    <li><a data-tab="tab-tn" class="active">Torneo</a></li>
                    <li><a data-tab="tab-eq">Comparación de equipos</a></li>
                    <li><a data-tab="tab-jg">Comparación de jugadores</a></li>
                  </ul>
                </div>
                <div class="cnt-tab-content">
                  <div id="tab-tn" class="tab-content active">
                   


<!--opta-widget widget="standings" competition="375" season="2018" date_from="2018-08-30" date_to="2018-12-31" template="normal" live="true" navigation="tabs" default_nav="1" side="combined" data_detail="full" show_key="false" show_crests="true" points_in_first_column="true" lose_before_draw="false" show_form="0" competition_naming="full" team_naming="full" date_format="dddd D MMMM YYYY" sorting="false" show_live="true" show_relegation_average="false" show_logo="false" title="TABLA DE POSICIONES" show_title="true" breakpoints="400" sport="football"><opta-widget sport="football" widget="fixtures" template="strip" live="false" competition="" season="" match="" team="" fixture_width="0" team_filter="" date_from="2018-08-30" date_to="2018-12-31" days_ahead="" days_before="" venue="" group="" matchday="" round="" match_status="all" grouping="date" show_grouping="false" navigation="" start_on_current="true" sub_grouping="date" show_subgrouping="false" order_by="date_ascending" show_crests="true" show_date="true" date_format="dddd D MMMM" month_date_format="MMMM" competition_naming="brief" team_naming="brief" team_link="" match_link="" pre_match="false" show_logo="true" breakpoints="400"></opta-widget></opta-widget-->
<opta-widget widget="fixtures" competition="375" season="2018" template="normal" live="false" date_from="2018-08-31" date_to="2018-12-31" show_venue="false" round="1,2,3,4" match_status="all" grouping="date" show_grouping="true" navigation="none" default_nav="1" start_on_current="true" sub_grouping="date" show_subgrouping="true" order_by="date_descending" show_crests="true" show_competition_name="true" date_format="ddd Do MMM" time_format="HH:mm" month_date_format="MMMM YYYY" competition_naming="full" team_naming="full" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>

<opta-widget widget="player_ranking" competition="375" season="2018" template="normal" navigation="tabs_more" show_crests="false" show_images="false" show_ranks="true" show_appearances="true" visible_categories="goals,shots,chances,passes,passes_long,assists,saves,cards_yellow,cards_red" limit="10" hide_zeroes="true" show_team_names="true" team_naming="full" player_naming="full" show_logo="false" title="RANKING DE JUGADORES" show_title="true" breakpoints="400" sport="football"></opta-widget>

<opta-widget sport="football" widget="team_ranking" competition="375" season="2018" template="graph" navigation="tabs" default_nav="1" show_crests="true" show_ranks="false" visible_categories="goals,goal_conversion,shots,shots_on_target,passes,passing_accuracy,tackles,tackles_accuracy,fouls,cards_yellow,cards_red,corners" limit="10" hide_zeroes="true" team_naming="full" title="RANKING DE EQUIPOS" show_logo="false" show_title="true" breakpoints="400"></opta-widget>

<opta-widget sport="football" widget="competition_stats" competition="375" season="2018" template="normal" show_crests="true" show_images="true" show_ranks="true" show_player_rankings="true" show_team_rankings="true" visible_stats="games_played,goals,shots,shots_on_target,corners,fouls,penalties,cards_yellow,cards_red,passes" limit="5" team_naming="full" player_naming="full" show_logo="false" title="DATOS DEL TORNEO" show_title="true" breakpoints="400"></opta-widget>

                  </div>
                  <div id="tab-eq" class="tab-content">

<!--opta-widget widget="team_compare" competition="375" season="2018" template="dual" navigation="tabs" default_nav="1" show_selects="true" show_crests="true" show_graphs="true" graph_style="relative" competition_naming="full" team_naming="full" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget-->

<opta-widget widget="team_compare" competition="375" season="2018" template="dual" navigation="tabs_more" default_nav="1" show_selects="true" show_crests="true" show_graphs="true" graph_style="full" competition_naming="full" team_naming="full" show_logo="false" title="COMPARADOR DE EQUIPOS" show_title="fALSE" breakpoints="400" sport="football"></opta-widget>

                    
                  </div>

                  <div id="tab-jg" class="tab-content">
                    
                     <opta-widget sport="football" widget="player_compare" competition="375" season="2018" template="dual" navigation="tabs" default_nav="1" show_selects="true" show_crests="true" show_images="false" competition_naming="full" team_naming="full" player_naming="full" show_logo="false" title="COMPARADOR DE JUGADORES" show_title="false" breakpoints="400"></opta-widget>

                  </div>
                </div>
              </div>





     
          </div>
        </div>
      </section>