<legend>Moderador</legend>
<?php if($this->session->flashdata('msg')): ?>
<div class="alert alert-success" role="alert">
  <?php echo $this->session->flashdata('msg'); ?>
</div>
<?php endif; ?>
<table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>username</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($moderadores as $moderador): ?>
    <tr>
      <td>#<?php echo $moderador->mid ?></td>
      <td>
        <?php echo $moderador->username ?>
      </td>
      <td>
        <a href="<?php echo base_url(); ?>admin/moderador/editar/<?php echo $moderador->mid ?>">Editar</a> | 
        <a href="<?php echo base_url(); ?>admin/moderador/eliminar/<?php echo $moderador->mid ?>">Eliminar</a>
      </td>
    <tr>
    <?php endforeach ?>
  </body>
<table>