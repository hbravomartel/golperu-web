<form action="<?php echo base_url(); ?>admin/moderador/save" method="post" class="form-horizontal">
  <fieldset>
  <legend>Nuevo Moderador</legend>
  <input class="hidden" name="mid" value="<?php echo @$moderador->mid;?>">
  <div class="control-group">
    <label class="control-label" for="username">Username:</label>
    <div class="controls">
      <input class="input-xlarge focused" name="username" id="username" type="text" placeholder="username" value="<?php echo @$moderador->username ?>" required="required" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="password">Password:</label>
    <div class="controls">
      <input class="input-xlarge focused" name="password" id="password" type="password" placeholder="clave" required="required" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="password">Rol:</label>
    <div class="controls">
      <select name="rid" id="rid">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
      </select>
    </div>
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Guardar</button>
    <button class="btn" id="cancelar">Cancelar</button>
  </div>
  <fieldset>
</form>

<script src="<?php base_url(); ?>assets/admin/js/jquery.js"></script>
<script>
    $('#cancelar').click(function(e){
      e.preventDefault();
      window.location.href="<?php echo base_url();?>admin/dashboard";
    });
</script>