<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Panel Admin</title>
    
    <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/select2/select2.css">
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/jquery-ui/jquery-ui.js"></script>


    <style>
      body { padding-top: 60px; }
      .nostyle{ list-style-type: none; }
    </style>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>admin/dashboard">Panel Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?php echo $this->load->view('admin/dashboard/menu', null, true) ?>
          </ul>
          <ul class="nav navbar-nav pull-right">
              <li class="dropdown">
                <a href="#"  data-toggle="dropdown" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username') ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url(); ?>admin/logout">Logout</a></li>
                </ul>
              </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <mp:Content />
    </div>

    <div id="footer">
      
      <mp:Footer />
    </div>

    <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
    <mp:Js />
  </body>
</html>
