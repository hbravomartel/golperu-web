<legend>Encuestas</legend>
<?php if($this->session->flashdata('msg')): ?>
<div class="alert alert-success" role="alert">
  <?php echo $this->session->flashdata('msg'); ?>
</div>
<?php endif; ?>
<table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>Programa</th>
      <th>Pregunta</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($encuestas as $encuesta): ?>
    <tr>
      <td>#<?php echo $encuesta->encuesta_id; ?></td>
      <td>
        <?php echo $encuesta->programa_nombre; ?>
      </td>
      <td>
        <?php echo $encuesta->encuesta_pregunta; ?>
      </td>
      <td>
      <?php if($encuesta->encuesta_estado == '1'){?>
        <a href="<?php echo base_url(); ?>admin/encuestas/activar/<?php echo $encuesta->encuesta_id ?>" class="btn btn-primary">Activar</a>
      <?php } else if($encuesta->encuesta_estado == '2'){?>
        <button class="btn btn-primary" disabled>Activado</button>
      <?php } ?>
        <a href="<?php echo base_url(); ?>admin/encuestas/eliminar/<?php echo $encuesta->encuesta_id ?>" class="btn btn-danger">Eliminar</a>
      </td>
    <tr>
    <?php endforeach ?>
  </body>
<table>