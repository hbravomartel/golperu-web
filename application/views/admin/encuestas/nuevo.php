﻿<form action="<?php echo base_url(); ?>admin/encuestas/procesar" method="post" class="form-horizontal">
  <fieldset>
  <legend>Nueva Encuesta</legend>
  <input class="hidden" name="mid" value="<?php echo @$moderador->mid;?>">

  <div class="control-group">
    <label class="control-label" for="nombre">Programa:</label>
    <div class="controls">
      <select name="programa_id" id="programa_id" required>
        <option></option>
        <?php foreach($programas as $programa):?>
        <option value="<?php echo $programa->programa_id;?>"><?php echo $programa->programa_nombre;?></option>
        <?php endforeach;?>
      </select>
    </div>
  </div>
  
  <div class="control-group">
    <label class="control-label" for="pregunta">Pregunta:</label>
    <div class="controls">
      <input class="input-xlarge focused" name="pregunta" id="pregunta" type="text" placeholder="pregunta" required="required" />
    </div>
  </div>

  <?php for($i = 1; $i <= 5; $i++):?>
  <div class="control-group">
    <label class="control-label" for="opcion_<?php echo $i;?>">Opción <?php echo $i;?>:</label>
    <div class="controls">
      <input class="input-xlarge focused" name="opcion[]" id="opcion[]" type="text" placeholder="opcion <?php echo $i;?>" />
    </div>
  </div>
  <?php endfor; ?>
  
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Guardar</button>
    <button class="btn" id="cancelar">Cancelar</button>
  </div>
  <fieldset>
</form>

<script src="<?php base_url(); ?>assets/admin/js/jquery.js"></script>
<script>
    $('#cancelar').click(function(e){
      e.preventDefault();
      window.location.href="<?php echo base_url();?>admin/dashboard";
    });
</script>