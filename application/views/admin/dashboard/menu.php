<?php $current = $this->uri->segment(2);
$menus = array('dashboard', 'moderador', 'imagen', 'encuestas');
$active = array();
foreach ($menus as $id => $menu) {
  if($current == $menu) $active[$id] = 'active';
}
?>
<li class="<?php echo @$active[0] ?>">
    <a href="<?php echo base_url(); ?>admin/dashboard/">Inicio</a>
</li>

<?php if($this->session->userdata('moderador_rol')<=2): ?>
  <li class="dropdown <?php echo @$active[1] ?>"><a data-toggle="dropdown" class="dropdown-toggle" href="#">Moderadores <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo base_url(); ?>admin/moderador/nuevo">Nuevo</a></li>
      <li><a href="<?php echo base_url(); ?>admin/moderador/index">Lista</a></li>
    </ul>
  </li>
<?php endif ?>


<?php if($this->session->userdata('moderador_rol')<=3): ?>
  <li class="dropdown <?php echo @$active[3] ?>"><a data-toggle="dropdown" class="dropdown-toggle" href="#">Encuestas <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo base_url(); ?>admin/encuestas/nuevo">Nuevo</a></li>
      <li><a href="<?php echo base_url(); ?>admin/encuestas/index">Lista</a></li>
    </ul>
  </li>
<?php endif ?>
