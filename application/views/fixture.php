<?php $default = base_url().'assets/img/default-portada.jpg'; ?>
 
    <main data-section="tab2" class="wrapper">
      <!-- HEADER DEL SITIO-->
      <?php $this->load->view("ads/leaderboard") ?>

      <header id="header" class="full-width">
        <div class="container cf">
          
          <?php $this->load->view("includes/logo"); ?>
          
          <div class="wrap-club-menu">   
            <?php $this->load->view("includes/clubs") ?>

            <?php $this->load->view("includes/nav") ?>
          </div>
        </div>
      </header>

      <?php $this->load->view("includes/ticker") ?>
      
      <!-- ELEMENTOS SOLO PARA VISTA MOBILE -->
      <?php $this->load->view("includes/nav-mobile") ?>
      <?php $this->load->view("includes/head-mobile") ?>


      <?php if (!empty($nav_subhome))
              echo $nav_subhome;
              ?>
<?php if ($this->uri->segment(2)=="fixture")
echo' <section class="seccion-head">';
echo'        <div class="container">';
echo'          <h2>Copa Movistar</h2>';
echo'          <div class="list">';
echo'            <ul>';
echo'              <li><a href="/copa-movistar/noticias">Noticias </a></li>';
echo'              <li><a href="/copa-movistar/goles-de-la-fecha">Goles</a></li>';
echo'              <li><a href="/copa-movistar/estadistica/">Estadísticas</a></li>';
echo'                    <li><a href="/copa-movistar/fixture/">Fixture</a></li>';
echo'            </ul>';
echo'          </div>';
echo'        </div>';
echo'      </section>';
  ?>

      <section class="principal">
        <div class="container cf nota">

          <?php $this->load->view("includes/sidebar") ?>

          
          <div class="content">
              <div class="wrap-nota">              
                  <h2 class="title">Fixture</h2>             
               </div>

             <?php /*<opta-widget sport="football" widget="fixtures" competition="375" season="2018" template="normal" live="true" team_filter="home" show_venue="false" match_status="result" grouping="date" show_grouping="true" default_nav="1" start_on_current="false" sub_grouping="date" show_subgrouping="false" order_by="date_ascending" show_crests="true" date_format="dddd D MMMM YYYY" time_format="hh:mm A" month_date_format="MMMM" competition_naming="full" team_naming="full" pre_match="false" show_live="true" show_logo="false" show_title="false" breakpoints="400"></opta-widget>*/?>


              <opta-widget widget="fixtures" competition="375" season="2018" template="normal" live="false" date_from="2018-08-31" date_to="2018-12-31" show_venue="false" round="1,2,3,4" match_status="all" grouping="date" show_grouping="true" navigation="none" default_nav="1" start_on_current="true" sub_grouping="date" show_subgrouping="true" order_by="date_descending" show_crests="true" show_competition_name="true" date_format="ddd Do MMM" time_format="HH:mm" month_date_format="MMMM YYYY" competition_naming="full" team_naming="full" pre_match="false" show_live="false" show_logo="false" show_title="true" breakpoints="400" sport="football"></opta-widget>


      
          </div>
        </div>
      </section>