<?php
class Encuesta_modelo extends CI_Model
{
  
  private $__tabla;

	function __construct()
	{
		parent::__construct();
    $this->load->database();
    $this->__tabla = 'encuestas';
    $this->load->library('read_htm');
    $this->load->helper(array('encuestas', 'validador'));
	}

  function save($data = array())
  {
    $tipo_id = $data['programa_id'] == '1' ? 2 : 1;
    $encuesta = array(
      'programa_id' => $data['programa_id'],
      'encuesta_pregunta' => $data['pregunta'],
      'tipoencuesta_id' => $tipo_id
    );

    $this->db->insert('encuestas', $encuesta);
    $encuesta_id = $this->db->insert_id();

    if(!empty($data['opciones']))
    {
      foreach($data['opciones'] as $opcion)
      {
        if(!empty($opcion))
        {
          $opciones[] = array(
            'opcion_nombre' => $opcion,
            'encuesta_id' => $encuesta_id
          );
        }
      }
    }

    $this->db->insert_batch('opciones', $opciones);

    return $this->db->affected_rows();
  }

  function getAll($limit = 10)
  {
    $this->db->select('encuesta_id, encuesta_pregunta, programa_nombre, encuesta_estado');
    $this->db->join('programas', 'programas.programa_id = encuestas.programa_id');
    $this->db->limit($limit);
    $this->db->where('encuesta_estado !=', '0');
    $this->db->order_by('encuesta_id', 'DESC');
    $query = $this->db->get($this->__tabla);
    return $query->result();
  }

  function deleteById($mid)
  {
    $data['encuesta_estado'] = '0';
    $this->db->where('encuesta_id', $mid);
    return $this->db->update($this->__tabla, $data);
  }

  function activar($mid)
  {
    $data['encuesta_estado'] = '2';
    $this->db->where('encuesta_id', $mid);
    return $this->db->update($this->__tabla, $data);
  }

  function get_last_encuesta()
  {
    $this->db->select('encuesta_id, encuesta_nombre, encuesta_pregunta, encuesta_id');
    $this->db->from('encuestas');
    $this->db->where('encuesta_estado', '2');
    $this->db->order_by('encuesta_id', 'desc');
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $encuesta = $query->row();
      $this->db->select('opcion_nombre, opcion_id');
      $this->db->from('opciones');
      $this->db->where('encuesta_id', $encuesta->encuesta_id);
      $opciones = $this->db->get()->result();

      $encuesta->opciones = $opciones;
      return $encuesta;
    }
    return FALSE;
  }

  function votar($opcion_id, $encuesta_id, $ip = NULL)
  {
    // primero la validacion del voto
    //$validacion = $this->_validar_voto($encuesta_id, $ip);
    $ip = $_SERVER['REMOTE_ADDR'];
    $validacion = TRUE;
    if($validacion === TRUE)
    {
      $votacion = array(
        'opcion_id' => $opcion_id,
        'encuesta_id' => $encuesta_id,
        'resultado_ip' => $ip,
        'resultado_fecha_registro' => date(DATE_ATOM)
      );
      $this->db->insert('resultados', $votacion);

      // Almacenar JSON
      $resultados = $this->get_resultados($encuesta_id);
      $this->updateJson($resultados);

      return TRUE; 
    }

    return FALSE;
  }

  function votar_convocable($opciones, $encuesta_id, $ip)
  {
    //$validacion = $this->_validar_voto_convocable($encuesta_id, $ip);
    $validacion = TRUE;
    if($validacion === TRUE)
    {
      $array_votos = array();
      foreach($opciones as $opcion)
      {
        $array_votos[] = array(
          'encuesta_id' => $encuesta_id,
          'opcion_id' => $opcion['opcion'],
          'voto' => $opcion['voto'],
          'resultado_ip' => $ip,
          'resultado_fecha_registro' => date(DATE_ATOM)
        );
      }
      $this->db->insert_batch('resultados_compuestos', $array_votos);

      return TRUE;
    }

    return FALSE;    
  }

  function get_programas()
  {
    $this->db->select('programa_id, programa_nombre');
    $this->db->from('programas');
    $this->db->where('programa_estado', '1');
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result();
    }
  }

  function get_resultados($encuesta_id)
  {
    $this->db->select('resultados.opcion_id, COUNT(resultados.opcion_id) as total, opcion_nombre');
    $this->db->group_by('resultados.opcion_id');
    $this->db->join('opciones', 'opciones.opcion_id = resultados.opcion_id'); 
    $this->db->where('resultados.encuesta_id', $encuesta_id);
    $this->db->order_by('total', 'desc'); 
    $query = $this->db->get('resultados');
    if($query->num_rows() > 0)
    {
      return $query->result();
    }
  }

  function get_resultados_convocables($encuesta_id)
  {
    $this->db->select('rc.opcion_id, voto, COUNT(voto) AS total, opcion_nombre');
    $this->db->from('resultados_compuestos rc');
    $this->db->join('opciones', 'opciones.opcion_id = rc.opcion_id');
    $this->db->group_by('opcion_id, voto');
    $this->db->where('rc.encuesta_id', $encuesta_id);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result();
    }
  }

  function get_resultados_by_programa($programa_id)
  {
    // conseguir el id de la ultima encuesta
    $encuesta = $this->get_encuesta_by_programa($programa_id);
    $encuesta_id = $encuesta['encuesta']->encuesta_id;

    $this->db->select('resultados.opcion_id, COUNT(resultados.opcion_id) as total, opcion_nombre');
    $this->db->group_by('resultados.opcion_id');
    $this->db->join('opciones', 'opciones.opcion_id = resultados.opcion_id');
    $this->db->where('resultados.encuesta_id', $encuesta_id);
    $this->db->order_by('total', 'desc'); 
    $query = $this->db->get('resultados');
    if($query->num_rows() > 0)
    {
      $resultados =  $query->result();
      foreach($resultados as $resultado)
      {
        $resultado->encuesta_pregunta = $encuesta['encuesta']->encuesta_pregunta;
      }      
      return $resultados;
    }
  }

  function get_encuesta_by_programa($programa_id)
  {
    $this->db->select('encuesta_pregunta, encuesta_id');
    $this->db->from('encuestas');
    $this->db->where('encuesta_estado', '2');
    $this->db->where('programa_id', $programa_id);
    $this->db->order_by('encuesta_id', 'desc');
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $encuesta = $query->row();
      $this->db->select('opcion_nombre, opcion_id');
      $this->db->from('opciones');
      $this->db->where('encuesta_id', $encuesta->encuesta_id);
      $opciones = $this->db->get()->result();

      $data['encuesta'] = $encuesta;
      $data['opciones'] = $opciones;

      return $data;
    }
    return FALSE;
  }

  function get_encuesta_by_programa_json($programa_id)
  {
    $encuestas = array();
    
    $this->db->select('e.encuesta_pregunta, e.encuesta_id, p.programa_id, p.programa_nombre');
    $this->db->from('encuestas as e');
    $this->db->join('programas as p', 'e.programa_id = p.programa_id');
    $this->db->where('e.encuesta_estado', '2');
    $this->db->where('e.programa_id', $programa_id);
    $this->db->order_by('e.encuesta_id', 'desc');
    $this->db->limit(10);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $encuestas = $query->result();

      foreach ($encuestas as $encuesta) {
        
        $this->db->select('opcion_nombre, opcion_id');
        $this->db->from('opciones');
        $this->db->where('encuesta_id', $encuesta->encuesta_id);
        $opciones = $this->db->get()->result();

        $encuesta->programa_nombre = $encuesta->programa_nombre;
        $encuesta->programa_id = $encuesta->programa_id;
        $encuesta->opciones = $opciones;
      }     

    }
    
    $encuestas = array_reverse($encuestas);
    mb_internal_encoding("UTF-8");
    $json = json_encode($encuestas, JSON_PRETTY_PRINT);
    // var_dump($json);
    return $json;
  }

  function get_encuesta_json()
  {
    $encuestas = array();
    
    $this->db->select('e.encuesta_pregunta, e.encuesta_id, p.programa_id, p.programa_nombre');
    $this->db->from('encuestas as e');
    $this->db->join('programas as p', 'e.programa_id = p.programa_id');
    $this->db->where('e.encuesta_estado', '2');
    $this->db->order_by('e.encuesta_id', 'desc');
    $this->db->limit(10);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $encuestas = $query->result();

      foreach ($encuestas as $encuesta) {
        
        $this->db->select('opcion_nombre, opcion_id');
        $this->db->from('opciones');
        $this->db->where('encuesta_id', $encuesta->encuesta_id);
        $opciones = $this->db->get()->result();

        $encuesta->programa_nombre = $encuesta->programa_nombre;
        $encuesta->programa_id = $encuesta->programa_id;
        $encuesta->opciones = $opciones;
      }     

    }
    
    $encuestas = array_reverse($encuestas);
    mb_internal_encoding("UTF-8");
    $json = json_encode($encuestas, JSON_PRETTY_PRINT);
    // var_dump($json);
    return $json;
  }

  function get_all_encuestas_by_programa($programa_id)
  {
    $this->db->select('encuesta_pregunta, encuesta_id');
    $this->db->from('encuestas');
    $this->db->where('encuesta_estado', '2');
    $this->db->where('programa_id', $programa_id);
    $this->db->order_by('encuesta_id', 'asc');
    $query = $this->db->get();

    if($query->num_rows() > 0)
    {
      $encuestas = $query->result();
      foreach($encuestas as $encuesta)
      {
        $this->db->select('opcion_nombre, opcion_id');
        $this->db->from('opciones');
        $this->db->where('encuesta_id', $encuesta->encuesta_id);
        $opciones = $this->db->get()->result();
        $data['opciones'][] = $opciones;
      }
      
      $data['encuestas'] = $encuestas;

      return $data;
    }
    return FALSE;
  }

  private function _validar_voto($encuesta_id, $ip)
  {
    $this->db->select('resultado_fecha_registro');
    $this->db->from('resultados');
    $this->db->where('resultado_ip', $ip);
    $this->db->where('encuesta_id', $encuesta_id);
    $this->db->order_by('resultado_id', 'desc');
    $this->db->limit(1);
    $query = $this->db->get();

    if($query->num_rows() > 0)
    {
      // existe al menos una entrada, hay que comprobar la fecha
      $fecha = $query->row();
      $dbdate = strtotime($fecha->resultado_fecha_registro);
      $date = date('Y-m-d', $dbdate);
      if (strtotime($date) === strtotime('today')) 
      {
        return FALSE;   
      }
    }
    // no hay registros del ip o es un día diferente
    return TRUE;
  }

  private function _validar_voto_convocable($encuesta_id, $ip)
  {
    $this->db->select('resultado_fecha_registro');
    $this->db->from('resultados_compuestos');
    $this->db->where('resultado_ip', $ip);
    $this->db->where('encuesta_id', $encuesta_id);
    $this->db->order_by('id_resultado', 'desc');
    $this->db->limit(1);
    $query = $this->db->get();

    if($query->num_rows() > 0)
    {
      // existe al menos una entrada, hay que comprobar la fecha
      $fecha = $query->row();
      $dbdate = strtotime($fecha->resultado_fecha_registro);
      if (time() - $dbdate < 10 * 60) 
      {
        return FALSE;    
      }
    }
    // no hay registros del ip o es un día diferente
    return TRUE;
  }

  function updateJson($respuestas)
  {
    $rutaEncuesta = 'json/encuestas/encuesta.json';

    $archivoEncuesta = fopen($rutaEncuesta, 'w');
    if ($archivoEncuesta) {
        fwrite($archivoEncuesta, json_encode($respuestas));
        fclose($archivoEncuesta);
    }
    return TRUE;
  }

  function get_encuesta_by_id($nid)
  {
    $noticia = new stdClass();
    $noticia->nid       = $this->read_htm->encuesta($nid, 'encuesta_id');
    $noticia->pregunta   = $this->read_htm->encuesta($nid, 'encuesta_pregunta');
    $noticia->programa_nombre    = stripslashes($this->read_htm->encuesta($nid, 'programa_nombre'));
    $noticia->programa_nid    = $this->read_htm->encuesta($nid, 'programa_nid');
    $noticia->opciones     = json_decode($this->read_htm->encuesta($nid, 'opciones'));

    // Para verificar que el objeto no este vacío, se convierte en un array temporal
    $tmp = (array) $noticia;

    if (array_filter($tmp))
    {
      return $noticia;
    }

    return FALSE;
  }

  public function get_last($categoria, $limit = null)
  {
    if(is_dir(programa_encuesta_path($categoria)) AND file_exists(programa_encuesta_path_nid($categoria)))
    {
      $categoria_nids = json_decode(file_get_contents(programa_encuesta_path_nid($categoria)));
      $notas = array();

      foreach($categoria_nids as $nid)
      {
        $notas[] = $this->get_encuesta_by_id($nid);
      }

      $last_nota = array_reverse($notas);
      if(!empty($limit))
      {
        return array_slice($last_nota, 0, $limit);
      }
      return $last_nota[0];

    }
    return FALSE;
  }


  // Function to get the client ip address
  function get_client_ip_server() {
      $ipaddress = '';
      if ($_SERVER['HTTP_CLIENT_IP'])
          $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
      else if($_SERVER['HTTP_X_FORWARDED_FOR'])
          $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
      else if($_SERVER['HTTP_X_FORWARDED'])
          $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
      else if($_SERVER['HTTP_FORWARDED_FOR'])
          $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
      else if($_SERVER['HTTP_FORWARDED'])
          $ipaddress = $_SERVER['HTTP_FORWARDED'];
      else if($_SERVER['REMOTE_ADDR'])
          $ipaddress = $_SERVER['REMOTE_ADDR'];
      else
          $ipaddress = 'UNKNOWN';
   
      return $ipaddress;
  }

}