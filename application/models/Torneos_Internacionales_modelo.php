<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Torneos_Internacionales_modelo extends CI_Model{

	private $seccion = 'torneos-internacionales';

	function __construct()
	{
		parent::__construct();
		$this->load->library('read_htm');
		$this->load->helper(array('noticias', 'validador', 'tag'));

	}

	function get_noticia_by_id($nid)
	{
		$noticia = new stdClass();
		$noticia->nid 			= $this->read_htm->noticia($nid, 'nid');
		$noticia->timestamp 	= $this->read_htm->noticia($nid, 'timestamp');
		$noticia->titular 		= stripslashes($this->read_htm->noticia($nid, 'titular'));
		$noticia->estado		= $this->read_htm->noticia($nid, 'estado');
		$noticia->categoria 	= $this->read_htm->noticia($nid, 'categoria');
		$noticia->seccion 		= $this->read_htm->noticia($nid, 'seccion');
		$noticia->linkseo 		= $this->read_htm->noticia($nid, 'linkseo');
		$noticia->desarrollo 	 	= stripslashes($this->read_htm->noticia($nid, 'desarrollo'));
		$noticia->tags 			= json_decode($this->read_htm->noticia($nid, 'tags'));
		$noticia->foto 			= json_decode($this->read_htm->noticia($nid, 'foto'));
		$noticia->fotoportada 	= validar_foto(json_decode($this->read_htm->noticia($nid, 'foto')), 'apaisado');
		$noticia->fotointerna 	= validar_foto(json_decode($this->read_htm->noticia($nid, 'foto')), 'mediano');
		$noticia->fotothumb 	= validar_foto(json_decode($this->read_htm->noticia($nid, 'foto')), '4x3');
		$noticia->fotos 		= json_decode($this->read_htm->noticia($nid, 'fotos'));
		$noticia->videos 		= validar_galeria($this->read_htm->noticia($nid, 'videos'));
		$noticia->audios 		= json_decode($this->read_htm->noticia($nid, 'audio'));
		$noticia->gif 			= json_decode($this->read_htm->noticia($nid, 'gif'));
		$noticia->categoriaUrl 	= '/'.substr($noticia->linkseo, 0, strrpos($noticia->linkseo, '/'));

		// Para verificar que el objeto no este vacío, se convierte en un array temporal
		$tmp = (array) $noticia;

		if (array_filter($tmp))
		{
			return $noticia;
		}

		return FALSE;
	}

	function get_noticias($limit = NULL)
	{
		$seccion = 'torneos_internacionales';
		if(file_exists(noticias_path_lista($seccion)))
		{
			$noticias = array_reverse(json_decode(file_get_contents(noticias_path_lista($seccion))));
		}
		
		if(!empty($noticias))
		{
			if(isset($limit))
			{
				$noticias = array_slice($noticias, 0, $limit);
			}
			
			foreach($noticias as $key => $value)
			{
				$value->titular = stripslashes($value->titular);
				$value->desarrollo = stripslashes($value->desarrollo);
				$value->fotoportada = validar_foto($value->foto, 'apaisado');
				$value->fotointerna = validar_foto($value->foto, 'mediano');
				$value->fotothumb = validar_foto($value->foto, '4x3');
				$value->linkseo = $value->linkseo;
				$value->categoriaUrl = '/'.substr($value->linkseo, 0, strrpos($value->linkseo, '/'));
			}

			return $noticias;
		}

		return FALSE;
	}

	function get_noticias_navbar()
	{
		if(file_exists(noticias_navbar()))
		{
			$noticias = json_decode(file_get_contents(noticias_navbar()));
			foreach($noticias as $value)
			{
				$value->titular = stripslashes($value->titular);
				$value->categoriaUrl = '/'.substr($value->linkseo, 0, strrpos($value->linkseo, '/'));
			}

			return $noticias;
		}
	}

	function get_noticias_by_categoria($categoria, $limit = NULL)
	{
		$seccion = $this->seccion;
		$nota_seccion = array();

		// Consigue los nids de las notas por sección y valida que exista la sección
		if (file_exists(categoria_path_nid($seccion, $categoria)))
		{
			$json_seccion = array_reverse(json_decode(file_get_contents(categoria_path_nid($seccion, $categoria))));
			$limit = isset($limit) ? $limit : 30;
			$json_seccion = array_slice($json_seccion, 0, $limit);

			// Validar que el json no esté vacío
			if (!empty($json_seccion))
			{
				// Busca cada nota por id
				foreach ($json_seccion as $nid)
				{
					$nota = $this->get_noticia_by_id($nid);
					if(!empty($nota))
					{
						$nota_seccion[] = $nota;
					}
				}
				return $nota_seccion;
			}

		}
		// Si la sección no existe, se devuelven un array vacío
		else
		{
			return array();
		}
	}

	function get_noticias_by_seccion($limit = NULL)
	{
		$seccion = $this->seccion;
		$nota_seccion = array();

		// Consigue los nids de las notas por sección y valida que exista la sección
		if (file_exists(seccion_path_nid($seccion)))
		{
			$json_seccion = array_reverse(json_decode(file_get_contents(seccion_path_nid($seccion))));
			$limit = isset($limit) ? $limit : 30;
			$json_seccion = array_slice($json_seccion, 0, $limit);

			// Validar que el json no esté vacío
			if (!empty($json_seccion))
			{
				// Busca cada nota por id
				foreach ($json_seccion as $nid)
				{
					$nota = $this->get_noticia_by_id($nid);
					if(!empty($nota))
					{
						$nota_seccion[] = $nota;
					}
				}
				return $nota_seccion;
			}

		}
		// Si la sección no existe, se devuelven un array vacío
		else
		{
			return array();
		}
	}

	function get_noticias_by_ids($nids, $tag_exclude = NULL)
	{
		if(!empty($nids))
		{
			$noticias = array();
			foreach($nids as $nid)
			{
				$nota = $this->get_noticia_by_id($nid);
				if(!empty($nota))
				{
					if(!empty($tag_exclude))
					{
						if(!empty($nota->tags))
						{
							// notas que no son de video
							if(!in_array($tag_exclude, $nota->tags))
							{
								$noticias[] = $nota;
							}
						}
						else
						{
							$noticias[] = $nota;
						}
					}
					else
					{
						$noticias[] = $nota;
					}
				}
			}
			return $noticias;
		}
		return FALSE;
	}

}