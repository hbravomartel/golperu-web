<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias_modelo extends CI_Model
{

   function __construct()
    {
        parent::__construct();
        $this->load->library('read_htm');
        $this->load->helper(array('categorias', 'validador'));
    }

    public function get_categoria_by_id($nid)
	{
	
        $categoria = new stdClass();
        $categoria->categoria_id           = $this->read_htm->categoria($nid, 'categoria_id');
        $categoria->categoria_nombre       = stripslashes($this->read_htm->categoria($nid, 'categoria_nombre'));
        $categoria->seccion_id              = $this->read_htm->categoria($nid, 'seccion_id');
        $categoria->competition_id          = $this->read_htm->categoria($nid, 'competition_id');
        $categoria->season                  = $this->read_htm->categoria($nid, 'season');
        $categoria->categoria_linkseo       = $this->read_htm->categoria($nid, 'categoria_linkseo');

 
        // Para verificar que el objeto no este vacío, se convierte en un array temporal
        $tmp = (array) $categoria;

        if (array_filter($tmp))
        {
            return $categoria;
        }

        return FALSE;  
	}

    function get_all($limit = NULL)
    {
 
        if(file_exists(categorias_path_lista()))
        {
            $categorias = array_reverse(json_decode(file_get_contents(categorias_path_lista())));
        }
        
        if(!empty($categorias))
        {
            if(isset($limit))
            {
                $categorias = array_slice($categorias, 0, $limit);
            }
            
            foreach($categorias as $key => $value)
            {
                $value->categoria_id = stripslashes($value->categoria_id);
                $value->categoria_nombre = stripslashes($value->categoria_nombre);
                $value->seccion_id = stripslashes($value->seccion_id);
                $value->competition_id = stripslashes($value->competition_id);
                $value->season = $value->season;
                $value->categoria_linkseo = $value->categoria_linkseo;

            }

            return $categorias;
        }

        return FALSE;
    }

    function get_info($seccion, $categoria)
    {

        if(file_exists(categoria_path_info($seccion, $categoria)))
        {
            $categorias = json_decode(file_get_contents(categoria_path_info($seccion, $categoria)));
        }

        if(!empty($categorias))
        {
            $categoria_id = $categorias->categoria_id;
            $categoria_object = $this->get_categoria_by_id($categoria_id);
            return $categoria_object;
        }

        return FALSE;
    }

  

}