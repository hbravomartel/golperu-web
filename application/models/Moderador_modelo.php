<?php
class Moderador_modelo extends CI_Model
{
  private $__tabla;

	function __construct()
	{
		parent::__construct();
    $this->load->database('encuestas');
    $this->__tabla = 'moderador';
	}

  function validCredential($username, $password)
  {
    $this->db->where('username', $username);
    $this->db->where('password', md5($password));
    $query = $this->db->get($this->__tabla);
    return $query->row();
  }

  function validRol($rol)
  {
    return ($this->session->userdata('moderador_rol') <= $rol);
  }

  function save($data = array())
  {
    $data['password'] = md5($data['password']); 
    if ($data['mid']>0) {
      $data['fech_update'] = date("Y-m-d H:i:s");
      $this->db->where('mid', $data['mid']);
      return $this->db->update($this->__tabla, $data);
    } else {
      return $this->db->insert($this->__tabla, $data);
    }
    return FALSE;
  }

  function getAll($limit = 10)
  {
    $this->db->limit($limit);
    $this->db->where('active', '1');
    $query = $this->db->get($this->__tabla);
    return $query->result();
  }

  function getById($mid)
  {
    $this->db->where('mid', (int)$mid);
    $query = $this->db->get($this->__tabla);
    return $query->row();
  }

  function deleteById($mid)
  {
    $data['active'] = '0';
    $this->db->where('mid', $mid);
    return $this->db->update($this->__tabla, $data);
  }
}
