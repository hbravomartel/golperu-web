<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_modelo extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('tag'));
	}

	function get_noticias_by_tag($tag) // nids de las noticias
	{
		if(is_file(tag_path_nid($tag)))
		{
			$nids = array_reverse(json_decode(file_get_contents(tag_path_nid($tag))));
			$nids = array_unique($nids);
			return $nids;
		}

		return FALSE;
	}

	function get_noticias_by_tags($tags) // array de tags, noticias
	{
		$nids = array();
		if(!empty($tags))
		{
			foreach($tags as $tag)
			{
				$nids[] = $this->get_noticias_by_tag($tag);
			}

			
			$arr = array_filter($nids);
			if(!empty($arr) AND is_array($nids[0]))
			{
				if(is_array($nids)){
					$result = call_user_func_array('array_merge', $nids);
					return $result;
				}
			}
		}

		return FALSE;
	}

	function generar_lista_tags($sitio_id)
	{
		// crear archivo html
		if(is_file(APPPATH.'views/includes/listatags.html'))
		{
			// se borra el ya existente
			unlink(APPPATH.'views/includes/listatags.html');
		}
		$tags = $this->get_all_tags($sitio_id);
		$html = '<ul id="buscador" class="buscador list-tag">';
		foreach($tags as $tag)
		{
			switch ($tag->tipotag_nombre) {
				case 'artista':
					$ruta = '/artistas'.'/';
					break;				
				default:
					$ruta = '/tags'.'/';
					break;
			}
			$tags_nombres[$ruta.$tag->tag_ruta] = $tag->tag_nombre;
			$html .= '<li class="list-item"><a href="'.$ruta.$tag->tag_ruta.'" class="list_hoy_tag">'.stripslashes($tag->tag_nombre).'</a></li>'; 
		} 
	    $html .= '</ul>';
	    $json = json_encode($tags_nombres);
	    $html .= '<script>var availableTags =';
	    $html .= print_r($json, true);
	    $html .= ';</script>';

	    file_put_contents(APPPATH.'views/includes/listatags.php', $html);

	    $this->generar_letras($tags);

	    return TRUE;
	}

	function generar_letras($tags)
	{
		foreach($tags as $key => $val)
		{
			$letra = ctype_alpha($val->tag_ruta[0]) ? strtoupper($val->tag_ruta[0]) : '0-9';
			$letras[$letra][] = $tags[$key];			
		}

		foreach($letras as $letra => $tags)
		{
			$html = '<ul id="buscador" class="buscador list-tag">';

			foreach($tags as $tag)
			{
				switch ($tag->tipotag_nombre) {
					case 'artista':
						$ruta = '/artistas'.'/';
						break;				
					default:
						$ruta = '/tags'.'/';
						break;
				}
				$html .= '<li class="list-item"><a href="'.$ruta.$tag->tag_ruta.'" class="list_hoy_tag">'.stripslashes($tag->tag_nombre).'</a></li>'; 
			} 

			$html .= '</ul>';
			file_put_contents(APPPATH.'views/includes/tags/'.$letra.'.html', $html);
		}
		
	}

	function get_all_by_tag($tag) //noticias
	{
		if(is_file(tag_path_nid($tag)))
		{
			$nids = array_reverse(json_decode(file_get_contents(tag_path_nid($tag))));
			return $nids;
		}

		return FALSE;
	}

	public function get_noticias_by_ids($nids, $tag = NULL)
	{
		// $nids = array_filter($nids);
		if(!empty($nids))
		{
			$noticias = array();
			$nids = array_unique($nids);
			foreach($nids as $nid)
			{
				$nota = $this->get_noticia_by_id($nid);
				if(!empty($nota))
				{
					if(!empty($tag))
					{
						if(!empty($nota->tags))
						{
							foreach ($nota->tags as $nota_tag) {
								if($nota_tag === $tag)
								{
									$noticias[] = $nota;
								}
							}
						}
						
					}
					
				}
			}

			// Ordenar Noticias
			$x = 0;
			foreach ($noticias as $noticia) {
			    $nid[$x] = $noticia->nid;
			    $timestamp[$x] = $noticia->timestamp;
			    $x++;
			}

			if(count($noticias) > 0)
			{
				array_multisort($timestamp, SORT_DESC, $noticias);
			}
			


			return $noticias;
		}
		return FALSE;
	}

	public function get_noticia_by_id($nid)
	{
		

		$noticia = new stdClass();
		$noticia->nid 			= $this->read_htm->noticia($nid, 'nid');
		$noticia->timestamp 	= $this->read_htm->noticia($nid, 'timestamp');
		$noticia->titular 		= stripslashes($this->read_htm->noticia($nid, 'titular'));
		$noticia->linkseo 		= stripslashes($this->read_htm->noticia($nid, 'linkseo'));
		$noticia->fotoportada 	= validar_foto(json_decode($this->read_htm->noticia($nid, 'foto')), 'apaisado');
		$noticia->fotomediano 	= validar_foto(json_decode($this->read_htm->noticia($nid, 'foto')), 'mediano');
		$noticia->fotointerna 	= validar_foto(json_decode($this->read_htm->noticia($nid, 'foto')), '4x3');
		$noticia->tags 			= json_decode($this->read_htm->noticia($nid, 'tags'));
	
		// Para verificar que el objeto no este vacío, se convierte en un array temporal
		$tmp = (array) $noticia;
		if (!empty(array_filter($tmp)))
		{
			if(!empty($noticia->nid))
			{
				return $noticia;
			}
			
		}
			
		return FALSE;
	}

	function get_nombre_by_tag($tag) // nids de las noticias
	{
		if(is_file(tag_path_nid($tag)))
		{
			$nombre = file_get_contents(tag_path_nombre($tag));
			return $nombre;
		}
		else
		{
			return humanize($tag, '-');
		}

		return FALSE;
	}


}