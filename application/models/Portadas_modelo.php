<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Portadas_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->load->library('read_htm');
		$this->load->helper(array('portada', 'validador'));
	}

	/**
	 * Devuelve todas las noticias en el JSON
	 * @return array datos del portada
	 */
	public function get_portadas($limit = NULL)
	{
		if(file_exists(portadas_path_lista()))
		{
			$portadas = array_reverse(json_decode(file_get_contents(portadas_path_lista())));
		}

		if(!empty($portadas))
		{
			if (isset($limit))
			{
				$portadas = array_slice($portadas, 0, $limit);
			}
			
			foreach($portadas as $key => $portada)
			{
					$portada->categoria = !empty($portada->categoria) ? $portada->categoria : $portada->seccion;
					$portada->titular = stripslashes($portada->titular);
					$portada->linkseo = limpiar_url($portada->linkseo);
					$portada->fotoportada = validar_foto($portada->foto, 'apaisado');
					$portada->fotointerna = validar_foto($portada->foto, 'mediano');
					$portada->fotothumb = validar_foto($portada->foto, 'thumb');
					$portada->fotocuadrada = validar_foto($portada->foto, '4x3');
					$portada->categoriaUrl = '/'.substr($portada->linkseo, 0, strrpos($portada->linkseo, '/'));
			}

			return $portadas;
				
		}

		return FALSE;			
	}
}